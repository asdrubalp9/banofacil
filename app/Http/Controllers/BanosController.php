<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Session;
use App\Bano;
use App\User;
use DB;
use App\Fondo;
use App\Reserva;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Mail;
use App\Mail\Mail as Template;
use App\Http\Traits\TelegramTrait;

class BanosController extends Controller
{
    //
    use TelegramTrait;
    private $booleans = [
        'jabon',
        'pestillo',
        'parking_para_bicicleta',
        'parking_para_carro',
        'agua_caliente',
        'secador_de_pelo',
        'enchufe',
        'toalla',
        'shampu'
    ];

    public function __construct(){
        
        $this->middleware(['auth','verified'])->except(['getAll','find']);
        
    }

    public function index(){
            
        return view('banos.index',['banos'=> Bano::where('user_id', Auth::user()->id )->get() ]);
    }

    public function create(){
            
        return view('banos.create',['bano'=>new Bano]);
    }

    public function store(Request $request){
        $user = Auth::user();
        //dd($request->fotos);
        if( count($user->Banos )  <= 5){
            $request = request()->validate([
                'costo'                 => ['required', 'integer','min:1', 'max:99999'],
                'tiempo_maximo'          => ['required', 'integer','min:1', 'max:60'],
                'direccion'             => ['required', 'string','min:10', 'max:100'],
                'hora_inicio'           => ['required', 'date_format:H:i'],
                'hora_fin'              => ['required', 'date_format:H:i'],
                'meta_direccion'        => ['required', 'string','min:3', 'max:100'],
                'jabon'                 => ['nullable', 'in:on'],
                'pestillo'              => ['nullable', 'in:on'],
                'parking_para_bicicleta'  => ['nullable', 'in:on'],
                'parking_para_carro'      => ['nullable', 'in:on'],
                'agua_caliente'          => ['nullable', 'in:on'],
                'secador_de_pelo'         => ['nullable', 'in:on'],
                'enchufe'               => ['nullable', 'in:on'],
                'toalla'                => ['nullable', 'in:on'],
                'shampu'                => ['nullable', 'in:on'],
                'shampu'                => ['nullable', 'in:on'],
                'fotos'                 =>['nullable','array'],
                'fotos.*'               => [ 'nullable','file','image:jpg,png', 'max:1500'],
                'lat'                   => ['required','regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'], 
                'long'                  => ['required','regex:/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/']
            ],[
                'tiempoMaximo.required' => 'El campo tiempo máximo es obligatorio.',
                'direccion.required'=>'El campo de dirección es obligatorio',
                'meta_direccion.required'=>'El campo de información adicional es obligatorio',
                //'fotos.*' => 
            ]);
            
            //dd( $request );
            foreach ($this->booleans as $key => $boolean){
                if( isset($request[$boolean]) ){
                    $request[$boolean] = 1;
                }else{
                    $request[$boolean] = 0;
                }    
            }
            unset($request['fotos']); //no eliminar para poder guardar en banos create
            
            //$request['user_id']  = Auth::user()->id;
            //$bano = Bano::create($request);
            $bano = Auth::user()->Banos()->create($request);
            //dd('asd');
            //dd( count (request()->fotos));
            foreach(request()->fotos as $file){  
                $bano->image()->create([
                    'url' => $file->store('uploads','public') 
                ]);                
            }
            /*
            $opciones = array(
                'http'=>array(
                  'method'=>"GET",
                  'header'=>"Accept-language: en\r\n" .
                            "Cookie: foo=bar\r\n"
                )
              );
            //*/  
            //$contexto = stream_context_create($opciones);
            //$text='Registraron un bano en '.$request['direccion'];
            //file_get_contents('https://api.telegram.org/bot1003788364:AAEQRO-YOKBTbQXN3Ilj38G9cSKj4aU54L4/sendMessage?chat_id=929814963&text='.$text, false, $contexto);
            $this->sendTelegram( 'Registraron un bano en '.$request['direccion'] );


            Session::flash('alert','success');
            Session::flash('msg','Se ha guardado el baño');
        }else{
            Session::flash('alert','danger');
            Session::flash('msg','Solo puedes tener un maximo de 2 baños, si te hacen falta mas, escribenos directamente.');
        }
        
        return redirect()->route( 'home' );
        ///return redirect()->route('home',['user'=> Auth::user()]) ;
        
    }

    public function update( Bano $bano){
        
        if($bano->user_id == Auth::user()->id ){

            $request = request()->validate([
                'costo'                 => ['required', 'integer','min:1', 'max:99999'],
                'tiempo_maximo'          => ['required', 'integer','min:1', 'max:255'],
                'direccion'             => ['required', 'string','min:10', 'max:100'],
                'hora_inicio'           => ['required', 'date_format:H:i'],
                'hora_fin'              => ['required', 'date_format:H:i'],
                'meta_direccion'        => ['required', 'string','min:3', 'max:100'],
                'jabon'                 => ['nullable', 'in:on'],
                'pestillo'              => ['nullable', 'in:on'],
                'parking_para_bicicleta'  => ['nullable', 'in:on'],
                'parking_para_carro'      => ['nullable', 'in:on'],
                'agua_caliente'          => ['nullable', 'in:on'],
                'secador_de_pelo'         => ['nullable', 'in:on'],
                'enchufe'               => ['nullable', 'in:on'],
                'toalla'                => ['nullable', 'in:on'],
                'shampu'                => ['nullable', 'in:on'],
                'fotos'                 =>['nullable','array'],
                'fotos.*'               => [ 'nullable','file','image:jpg,png', 'max:1500'],
                //'lat'                   => ['required','regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'], 
                //'long'                  => ['required','regex:/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/']
            ],[
                'tiempoMaximo.required' => 'El campo tiempo máximo es obligatorio.',
                'direccion.required'=>'El campo de dirección es obligatorio',
                'meta_direccion.required'=>'El campo de información adicional es obligatorio',
            ]);

            
            
            foreach ($this->booleans as $key => $boolean){
                if( isset($request[$boolean]) ){
                    $request[$boolean] = 1;
                }else{
                    $request[$boolean] = 0;
                }    
            }
            
            $bano->update( $request );

            //dd( count(request()->fotos));
            /*
            foreach(request()->fotos as $file){  
                $bano->image()->create([
                    'url' => $file->store('uploads','public') 
                ]);                
            }
            */
            Session::flash('alert','success');
            Session::flash('msg','Se ha actualizado la información del baño');
        }
        
        return redirect( )->route('home',['user'=> Auth::user()])->withErrors($validator, 'login'); ;
    }

    public function find(){
        return view('banos.find' );
    }

    public function contratar(Bano $id){
        $userInfo = User::with(['image','reviewsRecibido'])->where('id',$id->user_id)->first();
        

        $banos = DB::select('SELECT b.id, img.url, tiempo_maximo, b.costo, b.jabon, b.pestillo, b.parking_para_bicicleta, b.parking_para_carro, 
                                    b.agua_caliente, b.secador_de_pelo, b.enchufe, b.toalla, b.shampu, b.activo, u.genero,
                            CAST( ( 
                                            SELECT count(*) FROM reviews WHERE user_receptor =  b.user_id
                                        ) as CHAR) as cantCalificaciones
                            FROM 
                                    banos b
                            INNER JOIN
                                    users u ON u.id = b.user_id 
                            LEFT JOIN 
                                images img ON img.imageable_id = b.id   
                    
                            WHERE 
                            b.activo = 1
                            AND b.id = :id
                            AND img.image_type = 1

                        ',['id'=> $id->id]);

            foreach($banos as $key => $value){
                foreach($value as $k=>$v){
                    if( is_numeric($v) && $k != 'id' && $k != 'lat' && $k != 'long' && $k != 'costo' && $k != 'cantCalificaciones' && $k != 'promCalificaciones' ){
                        $banos[$key]->$k = ($v)?'Si':'No';
                    }
                }
            }

            $data = [];
            
            foreach($banos as $key => $value){
                //dd($value);
                
                $data['data'] =  $value;

                if($value->url){
                    $data['img'][] =  $value->url ;
                }
            }
            

                                
        if( count( Auth::user()->misFondos ) > 0 &&  Auth::user()->misFondos->last()->saldoActual > $id->costo  ){
            return view('banos.reserva',['bano'=>$id, 'imagenes'=>( isset($data['img']))?$data['img']:[], 'data'=>( isset($data['data']))?$data['data']:'' ]);
        }
        Session::flash('alert','danger');
        Session::flash('msg','No cuentas con fondos suficientes para contratar el sitio, Agrega dinero! <a href="'.route('pagos.add').'">haz clic aquí!</a>');
        return redirect()->back();
        
    }

    public function reservar(Bano $id, Request $request){
        
        $request = request()->validate([ 
            'fecha'=> ['required'],
            'hora'=> ['required'],
        ]);

        $fechaReserva = Carbon::parse($request['fecha'].' '. $request['hora']);
        
        $horaReserva = Carbon::createFromFormat('G:i',$request['hora']);
        $horaInicio = Carbon::createFromFormat('G:i',$id->hora_inicio);
        $horaFin = Carbon::createFromFormat('G:i',$id->hora_fin);
        $user = Auth::user();
        if( $user->misFondos->last()->saldoActual < $id->costo){
            Session::flash('alert','danger');
            Session::flash('msg','No cuentas con fondos suficientes para contratar el sitio, Agrega dinero! <a href="'.route('pagos.add').'">haz clic aquí!</a>');
            return redirect()->back();
        }

        if( $horaReserva >= $horaInicio && $horaReserva <=  $horaFin  ){
        }else{
            Session::flash('alert','danger');
            Session::flash('msg','No se puede a esta hora, esta fuera del horario permitido');
            return redirect()->back();
        }
        
        if( $fechaReserva <  Carbon::now() ){
            Session::flash('alert','danger');
            Session::flash('msg','No se puede reservar en el pasado');
            return redirect()->back();
        }
        //validar que ningun usuario lo este ocupando
        foreach( $id->reservas as $reserva){
            
            $mins = str_replace(' Mins.','',$id->tiempo_maximo);
            $rangoIni = Carbon::parse( $reserva->fecha_reserva )->subMinutes( intval($mins) ) ;
            $rangoFin = Carbon::parse( $reserva->fecha_reserva )->addMinutes( intval($mins) ) ;
            
            if( $fechaReserva >= $rangoIni && $fechaReserva <= $rangoFin){
                Session::flash('alert','danger');
                Session::flash('msg','No se puede a esta hora, Ya se encuentra reservado');
                return redirect()->back();
                
            }
            
        }
        
        //dd($fechaReserva);
        $mtime = microtime();
        $mtime = explode(' ', $mtime);
        $mtime = $mtime[1] + $mtime[0];
        $time = $mtime;
        //dd($time);
        //dd('stop');
        
        $reserva = Reserva::create([
            'user_pagador'=> $user->id,
            'user_receptor'=> $id->user_id,
            'bano_id' => $id->id,
            'costo'=>$id->costo,
            'confirmacion_pagador'=>0,
            'confirmacion_receptor'=>0,
            'fecha_reserva'=> $fechaReserva,
        ]);
        
        if($reserva){
            $saldoActual = $user->misFondos->last()->saldoActual - $id->costo;
            //dump('saldoActual ',$saldoActual );
            
            Fondo::create([
                'user_id'=>$user->id,
                'numero_transaccion'=> $time,
                'saldoActual'=>$saldoActual,
                'montoTransferencia'=>$id->costo,
                'tipo_transaccion'=>'pago'
            ]);

            $details['name'] = $user->name;
            $details['title'] = '¡Han reservado su baño!';
            $details['msg'] = '
                                <b>Datos de la reserva</b><br>
                                <b>Fecha y hora:</b> '.Carbon::parse($fechaReserva)->format('G:m d-m-Y') .'
                                <b>Nombre:</b> '.$user->name.' <br>
                                <b>genero:</b> '.$user->genero.' <br>
                                <b>telefono:</b> '.$user->telefono.' <br> 
                            ';
            //Mail::to('asdrubalp9@gmail.com' )->send(new Template($details));
            //dd($user->email);
            
            Mail::to( User::where('id',$id->user_id)->first()->email )->send(new Template($details));

        }
        Session::flash('alert','success');
        Session::flash('msg','La reserva fue realizada con exito, despues de usar el baño, no olvide calificar al usuario');
        
        return redirect()->route('home') ;

        //$disponibilidad = Bano::where();
    }

    public function getAll(){
        $banos = DB::select('SELECT 
            b.id, b.tiempo_maximo, tiempo_maximo, b.long, b.lat, b.costo, b.jabon, b.pestillo, b.parking_para_bicicleta, b.parking_para_carro, 
            b.agua_caliente, b.secador_de_pelo, b.enchufe, b.toalla, b.shampu, b.activo, u.genero, img.url,
                CAST( ( 
                    SELECT count(*) FROM reviews WHERE user_receptor =  b.user_id
                ) as CHAR) as cantCalificaciones,
                CAST( ( 
                     SELECT AVG(rating) FROM reviews WHERE user_receptor = b.user_id
                ) as SIGNED) as promCalificaciones
        FROM 
            banos b
        INNER JOIN
            users u ON u.id = b.user_id
        LEFT JOIN 
            images img ON img.imageable_id = b.id   
        WHERE 
            b.activo = 1
            and img.image_type = 1
        ');
        foreach($banos as $key => $value){
            foreach($value as $k=>$v){
                if( is_numeric($v) && $k != 'id' && $k != 'lat' && $k != 'long' && $k != 'costo' && $k != 'cantCalificaciones' && $k != 'promCalificaciones' ){
                    $banos[$key]->$k = ($v)?'Si':'No';
                }
            }
        }
        $data = [];
        
        foreach($banos as $key => $value){
            
            $data[$value->id]['data'] =  $value;

            if($value->url){
                $data[$value->id]['img'][] =  $value->url ;
            }
        }
        
        $points = [
            'userData'=>(Auth::user())? 1: 0,
            'Points' => $data
        ];
        return response()->json( $points );
    }

    public function usar(Bano $id, Request $request){
        
        $transaccion = Reserva::whereId($request->opc)->first();
        
        if( $transaccion->usuarioPagador->id !=  Auth::user()->id ){
            Session::flash('alert','danger');
            Session::flash('msg','Hubo un problema, intente luego nuevamente');            
            return redirect()->back();
        }
        
        return redirect()->action( 'ReviewController@create',['reserva' => $transaccion->id ,'tipo'=>'usar' ] );
        
    }

    public function prestar(Bano $id, Request $request){

        $transaccion = Reserva::whereId($request->opc)->first();
        
        if( $transaccion->user_receptor !=  Auth::user()->id ){
            Session::flash('alert','danger');
            Session::flash('msg','Hubo un problema, intente luego nuevamente');            
            return redirect()->back();
        }
        //dd($transaccion->user_pagador);
        return redirect()->action( 'ReviewController@create',[ 'reserva' => $transaccion->id,'tipo'=>'prestar' ] );
    }

    public function updateImgs(Bano $bano){


        if($bano->user_id == Auth::user()->id){
            //dd(request());
            $request = request()->validate([
                'fotos'                 =>['nullable','array'],
                'fotos.*'               => [ 'nullable','file','image:jpg,png', 'max:1500'],
            ],[
                'fotos.*' =>'Debe ser una imagen válida',
            ]);
            
            if(count($bano->image)+ count(request()->fotos) <= 5){
                foreach(request()->fotos as $file){  
                    $bano->image()->create([
                        'url' => $file->store('uploads','public') 
                    ]);                
                }
                Session::flash('alert','success');
                Session::flash('msg','Se han subido las fotos');
            }else{
                Session::flash('alert','danger');
                Session::flash('msg','Ha sobrepasado la cantidad de imagenes');
            }
            return redirect()->route( 'home' );
            
        }

    }
}
