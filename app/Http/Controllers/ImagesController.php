<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Auth;
use App\Image;
use Session;

class ImagesController extends Controller
{
    //
    public function __construct(){
        $this->middleware(['auth','verified']);
    }
    
    public function borrar(Request $request, Image $foto){
        
        if($foto->imageable->user_id == Auth::user()->id){
            //dd(storage_path('app/public/'.$foto->url));
            if( storage_path('app/public/'.$foto->url) ){
                
                $foto->delete();
                Session::flash('alert','success');
                Session::flash('msg','La imagen ha sido eliminada');
            }else{
                Session::flash('alert','danger');
                Session::flash('msg','La imagen no ha sido eliminada');
            }
            return redirect()->to($request->headers->get('referer'));

            
        }
    }

    public function carousel(){
        
        return view('banos.images',[ 'id'=> 2,'fotos' => Image::all()]);
    }
}
