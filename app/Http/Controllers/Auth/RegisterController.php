<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Image;

class RegisterController extends Controller
{
    
    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name'      => ['required', 'string', 'max:255'],
            'email'     => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'genero'    => ['required', 'string', 'max:255', 'in:Masculino,Femenino,Otro'],
            'telefono'  => ['required', 'string', 'max:255', 'unique:users','min:11'],
            'rut'  => ['required', 'string',  'unique:users','min:9'],
            'verificacion'  => ['required', 'file','image:jpg,png', 'max:3000'],
            'avatar'  => ['sometimes', 'image:jpg,png'],
            'password'  => ['required', 'string', 'min:8', 'confirmed'],
        ],[
            'verificacion.required'=>'Es de suma importancia tu foto con el rut para verificar el perfil.',
            'verificacion.image'=>'Solo se permite el formato jpg y png.'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'rut' => $data['rut'],
            'password' => Hash::make($data['password']),
            'genero' => $data['genero'],
            'telefono' => $data['telefono'],
        ]);
        
        if( request()->has('verificacion') ){
            
            $user->image()->create([
                'url'=> request()->verificacion->store('uploads','public'),
                'image_type'=>2,
            ]);
        }    
        if( request()->has('avatar')){
            $user->image()->create([
                'url'=> request()->avatar->store('uploads','public'),
                'image_type'=>3,
            ]);
        }    
        
        $opciones = array(
            'http'=>array(
              'method'=>"GET",
              'header'=>"Accept-language: en\r\n" .
                        "Cookie: foo=bar\r\n"
            )
          );
        
        $contexto = stream_context_create($opciones);
        $text='Se registro '.$data['name']. ', '.route('users.aprobarUsuario',['id'=>$user->id, 'clave'=> md5('asdrubalp9@gmail.com') ]);
        file_get_contents('https://api.telegram.org/bot1003788364:AAEQRO-YOKBTbQXN3Ilj38G9cSKj4aU54L4/sendMessage?chat_id=929814963&text='.$text, false, $contexto);
        //dd($user);
        return $user;
    }

    
}
