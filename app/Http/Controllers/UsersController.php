<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\Mail as Template;
use Illuminate\Support\Facades\Mail;
use Auth;
use Session;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
class UsersController extends Controller
{
    //
    public function __construct(){
        $this->middleware(['auth'])->except(['unverifiedAccount','aprobarUsuario','verify']);
    }
    
    public function aprobarUsuario( $id = null, $clave = null, Request $request){
        
        if( md5('asdrubalp9@gmail.com') ==  $clave ){
            
            if($id){
                $user[] = User::whereId($id)->first();
                $request->session()->put('clave', md5('asdrubalp9@gmail.com'));
                //dd('asd');
                return view('users.index',['users'=> $user ]);
            }

        }else{
            echo '.';
        }

    }
    public function index($id = null){
        $list = [
            'asdrubalp9@gmail.com',
            'asd@asd.com',
        ];
        if( in_array( Auth::user()->email, $list)  ){
            //dd($id);
            if($id){
                $user[] = User::whereId($id)->first();
                //dd('asd');
                return view('users.index',['users'=> $user ]);
            }else{
                return view('users.index',['users'=>User::whereNull('profile_verified_at')->with('Image')->get()]);
            }

        }else{
            return redirect()->route('home');
        }
    }

    public function verify(Request $request, $user){
        if(isset(Auth::user()->email) && Auth::user()->email == 'asd@asd.com' || $request->session()->get('clave') == md5('asdrubalp9@gmail.com') ){
            
            $user = User::whereId($user)->first();
            //dd($user->email);
            $user->ban = $request->opc;
            $user->profile_verified_at = Carbon::now();
            //dd($request->excusa);
            if($user->save() && $request->opc == 0){
                Session::flash('alert','success');
                Session::flash('msg','El usuario ha sido aprobado');
                $details['name'] = $user->name;
                $details['title'] = 'Su perfil en '.config('app.name', 'Laravel').' ha sido VERIFICADO!';
                $details['msg'] = $request->excusa;
                $details['msg'] = 'Ya puede utilizar nuestra app';
            }else{
                Session::flash('alert','danger');
                Session::flash('msg','El usuario ha sido BANEADOOOO');
                //mandar un correo de aceptacion o negacion
                $details['name'] = $user->name;
                $details['title'] = 'Su perfil en '.config('app.name', 'Laravel').' no ha podido ser verificado ';
                $details['msg'] = $request->excusa;
                $details['msg'] = $details['msg']. '<br> Si lo desea, puede replicar este correo en el caso que desee continuar con su registro';
                //Mail::to('asdrubalp9@gmail.com' )->send(new Template($details));
                //dd($user->email);
            }
            Mail::to($user->email )->send(new Template($details));
            if($request->session()->get('clave') == md5('asdrubalp9@gmail.com')){
                echo 'usuario validado!';
                die();
            }
            return redirect()->route('users.index');
            //return view('users.index',['users'=>User::whereNull('profile_verified_at')->with('Image')->get()]);

        }else{
            return redirect()->route('home');
        }
    }

    public function profile(){
        $user = Auth::user();
        
        return view('users.profile',['user'=>$user,'avatar'=> $user->image()->where('image_type','=',3)->first()]);
    }
    
    public function update(Request $request){
        
        $user = Auth::user();
        switch ($request->opc) {
            case 'telefono':
                # code...
                $request = request()->validate([
                    'telefono'  => ['required', 'string', 'max:255', 'unique:users','min:11'],
                    ]);
                    $user->telefono = $request['telefono'];
                    
                break;
            
            case 'avatar':
                $request = request()->validate([
                    'avatar'  => ['required', 'image:jpg,png'],
                    ]);
                $avatar = $user->image()->where('image_type','=',3)->first();
                //dd($avatar);
                if($avatar){
                    $avatar->delete();
                }
                $user->image()->create([
                    'url'=> request()->avatar->store('uploads','public'),
                    'image_type'=>3,
                ]);
                break;
            case 'password':
                $request = request()->validate([
                    'current_password' => ['required', 'string'],
                    'password'  => ['required', 'string', 'min:8', 'confirmed'],
                ],[
                    'current_password.required'=>'La contraseña actual es obligatoria'
                ]);
                //dd($request['current_password']);
                if(Hash::check($request['current_password'], $user->password)){
                    $user->password = Hash::make($request['password']);
                }
            break;
            
        }
        
        $user->save();
        
        Session::flash('alert','success');
        Session::flash('msg','Los cambios han sido realizados');
        
        return redirect()->route('users.profile');

    }

    public function unverifiedAccount(){
        return view('unverified');        
    }
    public function bannedAccount(){
        return view('banned');
    }

    public function transactionIndex(){
            
        return view('transacciones.index',['fondos'=>Auth::user()->misFondos]);
    }
}
