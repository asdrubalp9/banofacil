<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Bano;
use App\Review;
use App\Reserva;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','verified']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        //https://github.com/Laraveles/spanish        
        //$recibidas = DB::select("SELECT ");

/*

        foreach( $users as $user){
            dump($user->image[0]->imageable_id);
            dump($user->transaccionesPagadas);
            dump($user->transaccionesRecibidas);
        }
        //
        dd();
        */
       //$banosPorUsar =  
       $transRecibidas = DB::select("SELECT *,b.id as bano_id, r.id as reserva_id
                                    FROM users u
                                    
                                    INNER JOIN reservas r on r.user_pagador = u.id
                                    INNER JOIN banos b on b.user_id = u.id
                                    WHERE 1
                                    
                                    and r.confirmacion_receptor = 0
                                    and r.user_receptor = :id
       ",['id'=> Auth::user()->id ]);
       //dd($transRecibidas);
        return view( 'home',['recibidas'=> $transRecibidas] );
    }
}
