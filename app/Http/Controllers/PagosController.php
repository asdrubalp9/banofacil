<?php

namespace App\Http\Controllers;
use Exception;
use Illuminate\Http\Request;
use Auth;
use Session;
use App\Bano;
use App\User;
use DB;

use App\Reserva;
use App\Config;
use Carbon\Carbon;
use App\Fondo;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Mail;
use App\Mail\Mail as Template;


class PagosController extends Controller
{
    //
    protected $config;
    protected $FlowApi;

    public function __construct(){
        //$this->config = new \App\libs\flow\Config;
        $this->middleware(['auth','verified']);
        $this->FlowApi = new \App\libs\flow\FlowApi;
        
    }

    public function result(Request $request){
        //dump('resultado');
        if(!$request->token){
            $type = "danger";
            $resultado = 'La orden se encuentra rechazada';
            return redirect()->route('transacciones');
            
        }
        
        $params = array(
            "token" => $request->token
        );

        $serviceName = "payment/getStatus";
        
        $flowApi = new $this->FlowApi;

        $response = $flowApi->send($serviceName, $params, "GET");
                
        $saldoActual = Auth::user()->misFondos->last();

        $check = Fondo::where('numero_pago_api', $response['flowOrder'])->first();

        if($check){
            //dd('');
            $type="danger";
            $resultado = 'Esta orden ya se encuentra registrada';
            // ya el pago esta registrado y hay q evitar duplicaciones
            //regresar vista de que el pago ya fue registrado
        }else{
            switch($response['status']){
                /*
                https://www.flow.cl/docs/api.html#tag/refund
                    1 pendiente de pago
                    2 pagada
                    3 rechazada
                    4 anulada
                */
                case 1:
                    $type = "success";
                    $resultado = 'La orden esta pendiente de pago, estaremos revisando continuamente.';
                    
                break;
                case 3:
                    $type = "danger";
                    $resultado = 'La orden se encuentra rechazada';
                    
                    Auth::user()->misFondos()->create([
                        'saldoActual'       =>$saldoActual->saldoActual ,
                        'montoTransferencia'=> 0,
                        'numero_pago_api'   =>$response['flowOrder'],
                        'api'               => Config::first()->api_de_pago,
                        'num_orden_sopdar'  =>$response['commerceOrder'],
                        'monto_ingresado'   => $response['amount'],
                        'tipo_transaccion'  => 'Deposito',
                        'api_transaction_status'=>0,
                        'token'             =>$request->token
                    ]);

                break;
                case 2:
                    $type="success";
                    $resultado = 'Hemos abonado '.number_format($response['paymentData']['balance'],0,'','.'). ' CLP a su cuenta.';

                    if($saldoActual){
                        Auth::user()->misFondos()->create([
                            'saldoActual'=>$saldoActual->saldoActual + $response['paymentData']['balance'] ,
                            'montoTransferencia'=> $response['paymentData']['balance'] ,
                            'numero_pago_api'=>$response['flowOrder'],
                            'api' => Config::first()->api_de_pago,
                            'num_orden_sopdar'=>$response['commerceOrder'],
                            'monto_ingresado' => $response['amount'],
                            'tipo_transaccion' => 'Deposito',
                            'api_transaction_status'=>1,
                            'token'=>$request->token
                        ]);
                    }else{
                        Auth::user()->misFondos()->create([
                            'saldoActual'=> floor($response['paymentData']['balance']),
                            'montoTransferencia'=> $response['paymentData']['balance'],
                            'numero_pago_api'=>$response['flowOrder'],
                            'api' => Config::first()->api_de_pago,
                            'num_orden_sopdar'=>$response['commerceOrder'],
                            'monto_ingresado' => $response['amount'],
                            'tipo_transaccion' => 'Deposito',
                            'api_transaction_status'=>1,
                            'token'=>$request->token
                        ]);
                    }
                break;
                case 3:
                    //209351
                    $type="danger";
                    $resultado = 'hubo un error durante la transferencia';
                    Auth::user()->misFondos()->create([
                        'saldoActual'=> $saldoActual->saldoActual,
                        'montoTransferencia'=> 0,
                        'numero_pago_api'=> $response['flowOrder'],
                        'api' => Config::first()->api_de_pago,
                        'num_orden_sopdar'=>$response['commerceOrder'],
                        'monto_ingresado' => $response['amount'],
                        'tipo_transaccion' => 'Deposito',
                        'api_transaction_status'=>0,
                        'token'=>$request->token
                    ]);
                    
                break;
            }
        }
        $opciones = array(
            'http'=>array(
              'method'=>"GET",
              'header'=>"Accept-language: en\r\n" .
                        "Cookie: foo=bar\r\n"
            )
          );
        $contexto = stream_context_create($opciones);
        $text = preg_replace("/ /", "%20", 'PAGO: '. Auth::user()->name .' ha realizado lo siguiente: '.$resultado );
        file_get_contents('https://api.telegram.org/bot1003788364:AAEQRO-YOKBTbQXN3Ilj38G9cSKj4aU54L4/sendMessage?chat_id=929814963&text='.$text.' ', false, $contexto);
        
        Session::flash('alert', $type );
        Session::flash('msg', $resultado );
        
        return redirect()->route('transacciones');
        dd('fin');
        //dd($request->token);
    }
    
    public function index(){
        return view('pagos.index');
    }
    
    public function postAdd(Request $request){
        //dd($request->monto);
        $request = request()->validate([
            'monto'=>'required|integer|min:1000'
        ],[
            'monto.required'=>'El monto es obligatorio',
            'monto.integer'=>'No es un campo valido',
            'monto.min'=>'El monto tiene que ser mayor a 1000 clp',
        ]);
        $ordenDeCompra  = microtime(true);
        $optional = array(
            "rut" => Auth::user()->rut,
        );
        $optional = json_encode($optional);
        //Prepara el arreglo de datos
        //dd(Auth::user()->email);
        
        $params = array(
            "commerceOrder" => $ordenDeCompra,
            "subject" => "Abono en SOPDAR",
            "currency" => "CLP",
            "amount" => $request['monto'],
            "email" => Auth::user()->email,
            //"email" => 'asdrubalp9@gmail.com',
            "paymentMethod" => 9,
            "urlConfirmation" => route('pagos.confirm'),
            "urlReturn" => route('pagos.result'),
            "optional" => $optional
        );
        //Define el metodo a usar
        $serviceName = "payment/create";
        try {
            
            $flowApi = new $this->FlowApi;
            
            $response = $flowApi->send($serviceName, $params,"POST");
            //dd($response);
            $redirect = $response["url"] . "?token=" . $response["token"];
            return redirect()->to($redirect);
            header("location:$redirect");
        } catch (Exception $e) {
            //dd($response);
            //echo $e->getCode() . " - " . $e->getMessage();
            Session::flash('alert', 'danger' );
            Session::flash('msg', $response['message'] );
            return redirect()->route('pagos.index');

        }
    
    }



}
