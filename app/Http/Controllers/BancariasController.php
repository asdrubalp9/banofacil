<?php

namespace App\Http\Controllers;
use Auth;
use Session;
use App\User;
use App\CuentaBancaria;
use Illuminate\Http\Request;

class BancariasController extends Controller
{
    //
    public function __construct(){
        $this->middleware(['auth','verified']);
    }
    public function index(){
        //new CuentaBancaria;
        $banco = Auth::user()->cuentas()->first();
        if(!$banco){
            $banco = new CuentaBancaria;
        }
        return view('bancos.index',['banco'=> $banco ]);
    }

    public function store(Request $request){
        $banco = Auth::user()->cuentas()->first();
        //dd($banco);
        if(!$banco){
            $request = request()->validate([
                'nombre_titular'=> ['required','string','min:4'],
                'rut'=> ['required','string','min:7'],
                'banco'=> ['required','string'],
                'numero_de_cuenta'=> ['required','string'],
                'tipo_de_cuenta'=> ['required','string'],
                
            ]);
            Auth::user()->cuentas()->create($request);
            Session::flash('alert','success');
            Session::flash('msg','La cuenta bancaria ha sido creada');
        }else{
            $banco->nombre_titular = $request['nombre_titular'];
            $banco->rut = $request['rut'];
            $banco->banco = $request['banco'];
            $banco->numero_de_cuenta = $request['numero_de_cuenta'];
            $banco->tipo_de_cuenta = $request['tipo_de_cuenta'];
            
            $banco->save();
            Session::flash('alert','success');
            Session::flash('msg','La cuenta bancaria ha sido modificada');
        }   
        return redirect()->route('bancos.index');

    }
}
