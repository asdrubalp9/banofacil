<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use Session;
use App\User;
use App\CuentaBancaria;
use App\Http\Traits\TelegramTrait;

class RetiroController extends Controller
{
    //
    use TelegramTrait;

    public function __construct(){
        
        $this->middleware(['auth']);
    }

    public function index(){
        $banco = Auth::user()->cuentas()->first();


        return view('retiro.index', ['banco'=>$banco]);
    }

    public function create(Request $request){

        $request = request()->validate([
            'monto'=>['required','numeric','digits_between:0,1000000'],
            'contrasena' => ['required', 'password:web']
        ],[
            'monto.required'=> 'El monto es requerido',
            'monto.numeric' => 'El monto no es valido',
            'monto.digits_between' => 'El monto no es valido'
        ]);
        $banco = Auth::user()->cuentas()->first();
        if($banco){
            $saldoActual = Auth::user()->misFondos->last();
            ///dd($saldoActual);
            if( $request['monto'] < $saldoActual->saldoActual ){
                $intento = Auth::user()->Retiros()->create([
                    'numero_cuenta'=>$banco->numero_de_cuenta,
                    'nombre_banco'=>$banco->nombre_banco,
                    'nombre_titular'=>$banco->nombre_titular,
                    'monto' => $request['monto'],
                    'rut'=> $banco->rut,
                ]);
                Session::flash('alert','success');
                Session::flash('msg','Se esta gestionando su pago, en un lapso de 48 horas tendra su dinero disponible en su cuenta.');
                $mgs = 'Retiro por: '.$request['monto'].' al banco: ' ;
                $this->sendTelegram($msg);
                
                return redirect('home');
            }else{
                Session::flash('alert','danger');
                Session::flash('msg','El monto es demasiado alto.');
            }

        }
        return redirect('retiros.index');
    }
    public function ver(){
        if(Auth::user()->email == 'asd@asd.com'){
            /*
            $retiros = DB::table('users')
                        ->join('cuenta_bancarias', 'users.id', '=', 'cuenta_bancarias.users_id')
                        ->get();
                        */
            $retiros = User::with(['cuentas','misFondos'])->get();
            //dd($retiros);
            return view('retiro.ver',['retiros'=>$retiros]);

        }
    }
    public function add(Request $request){
        if(Auth::user()->email == 'asd@asd.com'){
        
            $request = request()->validate([
                'banco_emisor'=>['required', 'alpha_num'],
                'monto'=>['required','numeric','digits_between:0,1000000'],
                'fecha_transferencia' => ['required', 'date'],
                'numero_referencia' => ['required', 'alpha_num'],
                'user_id' => ['required', 'numeric'],
            ],[
                'monto.required'=> 'El monto es requerido',
                'monto.numeric' => 'El monto no es valido',
                'monto.digits_between' => 'El monto no es valido'
            ]);
            
            $user = User::with('cuentas','misFondos')->where('id', $request['user_id'])->first();
            
            if($user){
                $saldoActual = $user->misFondos->last();
                //dd($saldoActual->saldoActual);
                $datosBanco = $user->cuentas()->first();
                
                
                if( $request['monto'] <= $saldoActual->saldoActual ){
                    $intento = Auth::user()->Retiros()->create([
                        'numero_cuenta'=>$datosBanco->numero_de_cuenta,
                        'nombre_banco'=>$datosBanco->banco,
                        'nombre_titular'=>$datosBanco->nombre_titular,
                        'tipo_cuenta'=>$datosBanco->tipo_de_cuenta,
                        'numero_referencia'=>$request['numero_referencia'],
                        'monto' => $request['monto'],
                        'banco_emisor' =>$request['banco_emisor'],
                        'transferido'=>$request['fecha_transferencia'],
                        'rut'=> $datosBanco->rut,
                    ]);
                    //dd($intento);
                    if($intento){
                        //mandar un mail de q se hizo,
                        //mandar un telegram de q se hizo
                        //restar monto en tabla de monto para tener nuevo saldo
                        $user->misFondos()->create([
                            'numero_transaccion'=> $intento->id,
                            'saldoActual'       => floor( $saldoActual->saldoActual - $request['monto'] ),
                            'montoTransferencia'=> floor($request['monto'] ),
                            'tipo_transaccion'  =>'Transferencia a cuenta',
                            'numero_transaccion'=> $request['numero_referencia'],
                            //'banco_emisor'=> $request['banco_emisor'],
                        ]);
                        $mgs = 'Se deposito al banco, id: '.$intento->id ;
                        $this->sendTelegram($mgs);

                    }
                    
                    Session::flash('alert','success');
                    Session::flash('msg','Se esta gestionando su pago, en un lapso de 48 horas tendra su dinero disponible en su cuenta.');
                    
                    
                    return redirect()->route('retiros.ver');
                    // */
                }else{
                    Session::flash('alert','danger');
                    Session::flash('msg','El monto es demasiado alto.');
                }

            }
            return redirect()->route('retiros.ver');
        }
    }

    public function historial(){
        if(Auth::user()->email == 'asd@asd.com'){
            $data = User::with(['Retiros'])->get();
            return view('retiro.historial', ['datas'=>$data]);
        }
    }
}
