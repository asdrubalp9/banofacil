<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Session;
use App\Bano;
use App\User;
use DB;
use App\Fondo;
use App\Reserva;
use Carbon\Carbon;
use App\Review;
use App\Config;
use App\Profit;

//use Illuminate\Support\Facades\Input;
class ReviewController extends Controller
{
    //
    public function __construct(){
        $this->middleware(['auth','verified']);
    }

    public function index(){
        $calificacionesPorUsar = DB::select('SELECT 
                                                    u.name, revs.rating, revs.comentario,  revs.visto_receptor, revs.id, revs.created_at
                                                FROM 
                                                    reviews revs
                                                INNER JOIN 
                                                    reservas res ON res.id = revs.reserva_id
                                                INNER JOIN
                                                    users u  ON u.id = revs.user_emisor
                                                WHERE 
                                                    revs.user_receptor = :idUser
                                                    AND res.user_receptor = :idUser2
                                                    ',[':idUser'=> Auth::user()->id, ':idUser2'=> Auth::user()->id]);
        
        $calificacionesPorPrestar = DB::select('SELECT 
                                    u.name, revs.rating, revs.comentario,  revs.visto_receptor, revs.id, revs.created_at
                                FROM 
                                    reviews revs
                                INNER JOIN 
                                    reservas res ON res.id = revs.reserva_id
                                INNER JOIN
                                    users u  ON u.id = revs.user_emisor
                                WHERE 
                                    revs.user_receptor = :idUser
                                    AND res.user_pagador = :idUser2 ;
                                ',[':idUser'=> Auth::user()->id, ':idUser2'=> Auth::user()->id]);
        
        /*
        $calificacionesPorUsar = Review::where('user_receptor', '=', Auth::user()->id )
                                        ->where('activo','=',1)
                                        ->with('usuarioReceptor')
                                        ->get();
                                        /*
                                        // */
        
        /*
        $calificacionesPorPrestar = Review::where('user_repector', '=', Auth::user()->id )
        ->where('visto_emisor', '=', 0)
        ->orWhere('user_receptor','=',Auth::user()->id )
        ->where('visto_receptor','=',0)
        ->with('usuarioEmisor')
        ->get();
        */
        // $prestarBano = Review::where('user_receptor','=',Auth::user()->id)
        //                         ->orderBy('id','DESC')->with('usuarioReceptor')->get();
        //$usarBano = Review::where('user_emisor','=',Auth::user()->id)->orderBy('id','DESC')->with('usuarioEmisor')->get();
        return view('reviews.index',[ 'calificacionesPorUsar'=>$calificacionesPorUsar, 'calificacionesPorPrestar'=>$calificacionesPorPrestar ]);
        
    }
    public function visto(Review $id){
        //dd($id->user_emisor);
        if( $id->user_emisor == Auth::user()->id || $id->user_receptor == Auth::user()->id ){
            
            $id->visto_receptor = 1;
            $id->save();
            Session::flash('alert','success');
            Session::flash('msg','La calificación fue marcada como vista');
            return redirect()->back();
        }

    }
    public function create(Request $request){
        //dd('asd',$request->reserva);

        $transaccion = Reserva::whereId($request->reserva)->first();
        //dd($transaccion);
        if($request->tipo == 'prestar'){
            $var = 'user_pagador';
            $id = $transaccion->user_pagador;
        }else{
            $var = 'user_receptor';
            $id = $transaccion->user_receptor;
        }

        $userInfo = DB::select('SELECT  u.genero, u.name, img.url,
                            CAST( ( 
                                            SELECT AVG(rating) FROM reviews WHERE user_receptor =  b.user_id
                                        ) as CHAR) as cantCalificaciones
                            FROM 
                                    banos b
                            INNER JOIN
                                    users u ON u.id = b.user_id 
                            LEFT JOIN 
                                images img ON img.imageable_id = b.id   
                    
                            WHERE 
                            b.activo = 1
                            AND u.id = :id
                            AND img.image_type = 3

                        ',['id'=> $id]);
        $userInfo = $userInfo[0];
        //dd($userInfo);
        return view('reviews.create',[ 'id'=> $id, 'input' => $var, 'trans'=> $transaccion->id, 'tipo'=> $request->tipo, 'userInfo' => $userInfo ]);
        //return view('reviews.create',['user'=> Auth::user()]);
    }
    public function store(Request $request){

        //dd($request);
        $request = request()->validate([
            'rating'                 => ['required', 'integer','min:1', 'max:5'],
            'transaccion'          => ['required', 'integer','min:1'],
            'id'                => ['required', 'integer','min:1'],
            'tipo'                => ['required', 'string','in:usar,prestar'],
            'comentario'            =>['required','string', 'min:1', 'max:255'],
        ]);
        $reserva = Reserva::whereId( $request['transaccion'] )->first();
        
        if($request['tipo'] == 'usar'){ //calificar al que presta
            //dd('prestar');
            if($reserva->user_receptor != Auth::user()->id && $reserva->user_pagador == $request['id'] ){
                Session::flash('alert','danger');
                Session::flash('msg','Hubo un problema, intente luego nuevamente');            
                return redirect()->back();
            }
            $reserva->confirmacion_pagador = 1;
            $receptorDeCalificacion = $reserva->user_receptor;
            $emisorDeCalificacion =  Auth::user()->id;
            
            
        }else{//calificar al que usa
            if($reserva->user_pagador != Auth::user()->id && $reserva->user_receptor == $request['id']){
                Session::flash('alert','danger');
                Session::flash('msg','Hubo un problema, intente luego nuevamente');            
                return redirect()->back();
            }
            $reserva->confirmacion_receptor = 1;
            $receptorDeCalificacion =  $reserva->user_pagador;
            $emisorDeCalificacion = $reserva->user_receptor;
        }
        
        //guardar review
        $buscar = Review::where('reserva_id','=', $reserva->id)
                        ->where('user_emisor' , '=', Auth::user()->id)
                        ->get();
                        
        
        if( count($buscar) == 0){
            $reserva->save();
            
            $review = Review::create([
                'user_emisor' =>$emisorDeCalificacion,
                'user_receptor' =>$receptorDeCalificacion,
                'reserva_id' =>$reserva->id,
                'rating' =>$request['rating'],
                'comentario' =>$request['comentario'],
            ]);

            
            if($review){
                //dd($reserva);
                if( ($reserva->confirmacion_pagador) && ($reserva->confirmacion_receptor) ){
                    //$bonificar
                    $userBono = User::where('id',$reserva->user_receptor)->first();
                    $plata = $userBono->misFondos->last();
                    $config = Config::first();
                    if($config->tipo_profit = '%'){
                        $profit =  $reserva->costo * ( $config->monto_profit / 100 );
                    }

                    Profit::create([
                        'reserva_id'=>$reserva->id,
                        'monto'=> $profit
                    ]);
                    
                    if($plata){
                        $userBono->misFondos()->create([
                            'numero_transaccion'=> $reserva->id,
                            'saldoActual'       => floor( $reserva->costo + $plata->saldoActual - $profit ),
                            'montoTransferencia'=> floor($reserva->costo - $profit),
                            'tipo_transaccion'  =>'Ingreso',
                        ]);
                    }else{
                        $userBono->misFondos()->create([
                            'numero_transaccion'=> $reserva->id,
                            'saldoActual'       => floor($reserva->costo - $profit),
                            'montoTransferencia'=> floor($reserva->costo - $profit),
                            'tipo_transaccion'  =>'Ingreso',
                        ]);
                    }

                    

                    if($reserva->user_pagador == Auth::user()->id ) {
                        Session::flash('alert','success');
                        Session::flash('msg','Se ha calificado al usuario, hemos debitado de su cuenta el costo del baño por su uso.');
                    }else{
                        Session::flash('alert','success');
                        Session::flash('msg','Se ha calificado al usuario, hemos bonificado a su cuenta '.$reserva->costo.' por el uso del baño.');
                    }
                    //transferir plata
                }else{
                    Session::flash('alert','success');
                    Session::flash('msg','El usuario ha sido calificado, muchas gracias por su colaboración');
                }
                //dd('stop');
                return redirect()->route('home');
            }
        }else{
            Session::flash('alert','danger');
            Session::flash('msg','Ya habia realizado esta calificación');
            return redirect()->route('home');
        }
        // */

        

    }
}
