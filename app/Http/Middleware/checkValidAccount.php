<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Session;
use Request;
class checkValidAccount
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        if(Auth::check()){    
            if( (Auth::user()->profile_verified_at) ){
                return $next($request);
            }
            if(Request::path() != 'perfil/sin-verificar' &&   strpos( Request::path(), '/verify/' ) != 5 ){
                
                //dd( Request::path());
                Auth::logout();
                return redirect('/perfil/sin-verificar');
            }
        }
        //dd('paso');
        return $next($request);
        
    }
}
