<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Session;
use Request;

class checkForBannedUsers
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //dd(Auth::user());
        if(Auth::check()){    
            if( Auth::user()->ban && Request::path() != 'perfil/banned'){
                return redirect('/perfil/banned');
            }
            /*
            if(Request::path() != 'perfil/banned'){
                Auth::logout();
                return redirect('/perfil/banned');
            }
            */
        }
        return $next($request);
    }
}
