<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    //
    protected $guarded = [];
    public function reviews()
    {
        return $this->hasMany(Category::class);
    }
    public function childrenReviews(){
        return $this->hasMany(Category::class)->with('reviews');
    }

    public function usuarioEmisor(){
        return $this->belongsTo(User::class,'user_emisor', 'id');
    }
    public function usuarioReceptor(){
        return $this->belongsTo(User::class,'user_receptor', 'id');
    }

}
