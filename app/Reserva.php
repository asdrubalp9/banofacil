<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
class Reserva extends Model
{
    //
    protected $guarded = [];
    protected $dates= ['fecha_reserva'];

    public function transaccionesPagadas(){
        return $this->belongsTo(User::class, 'id', 'user_pagador');
    }
    
    public function transaccionesRecibidas(){
        return $this->belongsTo(User::class, 'id', 'user_receptor');
    }

    public function bano(){
        return $this->belongsTo(Bano::class);
    }

    public function usuario(){
        return $this->belongsTo(User::class,'user_receptor');
    }
    public function usuarioPagador(){
        return $this->belongsTo(User::class,'user_pagador');
    }
    
    

    // public function getFechaReservaAttribute(){
    //     $val = Carbon::createFromFormat( 'd-m-Y G:i', $this->fecha_reserva ) ;
    //     return $val;
    // }
}
