<?php
/**
 * Clase para Configurar el cliente
 * @Filename: Config.class.php
 * @version: 2.0
 * @Author: flow.cl
 * @Email: csepulveda@tuxpan.com
 * @Date: 28-04-2017 11:32
 * @Last Modified by: Carlos Sepulveda
 * @Last Modified time: 28-04-2017 11:32
 */
namespace App\libs\flow; 
use Exception;
 $COMMERCE_CONFIG = array(
 	"APIKEY" => "5AB19FFD-F699-42FB-873D-7AC2725LD6CA", // Registre aquí su apiKey
 	"SECRETKEY" => "d159b578dc17ef060773df325d352d8b3ba0964d", // Registre aquí su secretKey
    "APIURL" => "https://sandbox.flow.cl/api", // Producción EndPoint o Sandbox EndPoint
    //"APIURL" => "https://www.flow.cl/api", // Producción EndPoint o Sandbox EndPoint
 	"BASEURL" => "https://www.micomercio.cl/apiFlow" //Registre aquí la URL base en su página donde instalará el cliente
 );
 
 class Config {
 	
	static function get($name) {
		global $COMMERCE_CONFIG;
		if(!isset($COMMERCE_CONFIG[$name])) {
			throw new Exception("The configuration element thas not exist", 1);
		}
		return $COMMERCE_CONFIG[$name];
	}
 }