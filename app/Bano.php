<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bano extends Model
{
    //
    protected $guarded = [];
    public function Usuario(){
        return $this->belongsTo(User::class);
    }
    public function reservas(){
        return $this->hasMany(Reserva::class);
    }

    public function getParkingParaBicicletaAttribute($value){
        return ($value)?'Sí':'no';
    }
    public function getParkingParaCarroAttribute($value){
        return ($value)?'Sí':'no';
    }

    public function getToallaAttribute($value){
        return ($value)?'Sí':'no';
    }

    public function getPestilloAttribute($value){
        return ($value)?'Sí':'no';
    }

    public function getShampuAttribute($value){
        return ($value)?'Sí':'no';
    }
    public function getSecadorDePeloAttribute($value){
        return ($value)?'Sí':'no';
    }
    public function getJabonAttribute($value){
        return ($value)?'Sí':'no';
    }
    public function getAguaCalienteAttribute($value){
        return ($value)?'Sí':'no';
    }
/*
    public function getTiempoMaximoAttribute($value){
        
        $str =  ($value > 0)?' Mins.':' Min.';
        $value = $value.$str;
        return $value;
    }
*/
    public function getEnchufeAttribute($value){
        return ($value)?'Sí':'no';
    }
    public function image()
    {
        return $this->morphMany(Image::class, 'imageable');
    }
    
}
