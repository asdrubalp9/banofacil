<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

use App\Review;
use Session;
use Auth;
use View;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //

        Schema::defaultStringLength(191);

        View::composer(['home','banos.index','transacciones.index','pagos.index','retiros.index' ],function($view){
            $saldoActual = Auth::user()->misFondos->last();
            $saldoActual = ( !empty($saldoActual))? 'Tienes '.number_format($saldoActual->saldoActual,0,'','.'). ' CLP':'No posees fondos';
            $user = Auth::user();
            $view->with( ['user'=> $user,'saldoActual'=> $saldoActual] );
        });
        
        View::composer(['partials.nav'],function($view){
            
            if( auth()->check() ){        
                $notificaciones = Review::where('user_receptor', '=', Auth::user()->id )
                                ->where('visto_receptor','=',0)
                                ->get();
                $view->with('navNotif', count($notificaciones));
    
                
            }
        });
    }
}
