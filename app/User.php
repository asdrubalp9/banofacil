<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;


class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'telefono', 'genero'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function Banos(){
        return $this->hasMany(Bano::class);
    }

    public function transaccionesPagadas(){
        return $this->hasMany(Reserva::class, 'user_pagador', 'id');
    }
    public function transaccionesRecibidas(){
        return $this->hasMany(Reserva::class, 'user_receptor', 'id');
    }
    public function misFondos(){
        return $this->hasMany(Fondo::class);
    }
    public function reservas(){
        return $this->hasMany(Reserva::class);
    }

    public function reviewsRecibido(){
        return $this->hasMany(Review::class,'user_emisor');
    }
    public function reviewsEmitidos(){
        return $this->hasMany(Review::class,'user_receptor');
    }

    public function image()
    {
        return $this->morphMany(Image::class, 'imageable');
    }
    
    public function cuentas(){
        return $this->hasMany(CuentaBancaria::class);
    }

    public function Retiros(){
        return $this->hasMany(Retiro::class);
    }

}
