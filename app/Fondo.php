<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fondo extends Model
{
    //
    protected $guarded = [];
    public function usuario(){
        return $this->belongsTo(User::class);
    }
    
}
