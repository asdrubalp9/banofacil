<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Retiro extends Model
{
    //
    protected $guarded =[];
    public function Usuarios(){
        return $this->belongsTo(User::class);
    }
}
