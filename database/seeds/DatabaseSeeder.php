<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        // $this->call(UsersTableSeeder::class);
        App\User::create([
            'id'=>1,
            'name'=>'Asdrubal Perez',
            'email'=>'asd@asd.com',
            'email_verified_at'=>'2019-11-22 14:12:06',
            'profile_verified_at' => '2019-11-22 14:12:06',
            'genero'=>'Masculino',
            'password'=>Hash::make('123'),
            'telefono'=>'123'
        ]);
        App\User::create([
            'id'=>2,
            'name'=>'Mirenchitu',
            'email'=>'qwe@qwe.com',
            'profile_verified_at' => '2019-11-22 14:12:06',
            'email_verified_at'=>'2019-11-22 14:12:06',
            'genero'=>'Femenino',
            'password'=>Hash::make('123'),
            'telefono'=>'321'
        ]);
        App\User::create([
            'name'=>'Gregorio Perez',
            'email'=>'001gregorio@gmail.com',
            'email_verified_at'=>'2019-11-22 14:12:06',
            'profile_verified_at' => '2019-11-22 14:12:06',
            'genero'=>'Masculino',
            'password'=>Hash::make('123'),
            'telefono'=>'56953000342'
        ]);
        App\User::create([
            'name'=>'Chiki tirribli',
            'email'=>'001adrianarivas@gmail.com',
            'email_verified_at'=>'2019-11-22 14:12:06',
            'profile_verified_at' => '2019-11-22 14:12:06',
            'genero'=>'Masculino',
            'password'=>Hash::make('123'),
            'telefono'=>'56998792482'
        ]);
        App\User::create([
            'name'=>'Mirentxu Ullloa',
            'email'=>'mirentxu.ulloa@gmail.com',
            'email_verified_at'=>'2019-11-22 14:12:06',
            'profile_verified_at' => '2019-11-22 14:12:06',
            'genero'=>'Masculino',
            'password'=>Hash::make('123'),
            'telefono'=>'56998792480'
        ]);
        App\User::create([
            'name'=>'Alejandro Chimbete',
            'email'=>'piux777@gmail.com',
            'email_verified_at'=>'2019-11-22 14:12:06',
            'profile_verified_at' => '2019-11-22 14:12:06',
            'genero'=>'Masculino',
            'password'=>Hash::make('123'),
            'telefono'=>'56123123123'
        ]);
        App\User::create([
            'name'=>'Artureto eto',
            'email'=>'arturoperez001@gmail.com',
            'email_verified_at'=>'2019-11-22 14:12:06',
            'profile_verified_at' => '2019-11-22 14:12:06',
            'genero'=>'Masculino',
            'password'=>Hash::make('123'),
            'telefono'=>'563123123312'
        ]);
        App\Image::create([
            'image_type'=> 1,
            'imageable_id'=>1,
            'url' => 'uploads/2WxCFNUCNmBvxrzD0pQqeAMKKp9abLpafZ0aOtSf.jpeg',
            'imageable_type'=>'App\Bano',
        ]);
        App\Image::create([
            'image_type'=> 3,
            'imageable_id'=>3,
            'url' => 'uploads/KZ0oJVfnluBAvlC36smTtn6C2CvGaqM77FTGn4yy.jpeg',
            'imageable_type'=>'App\User',
        ]);
        App\Image::create([
            'image_type'=> 2,
            'imageable_id'=>3,
            'url' => 'uploads/KZ0oJVfnluBAvlC36smTtn6C2CvGaqM77FTGn4yy.jpeg',
            'imageable_type'=>'App\User',
        ]);
        App\Image::create([
            'image_type'=> 3,
            'imageable_id'=>4,
            'url' => 'uploads/9cqNeSjtZgGTvk1Em9VKt19qOy6t8bXjQNYmPRoV.jpeg',
            'imageable_type'=>'App\User',
        ]);
        App\Image::create([
            'image_type'=> 2,
            'imageable_id'=>4,
            'url' => 'uploads/9cqNeSjtZgGTvk1Em9VKt19qOy6t8bXjQNYmPRoV.jpeg',
            'imageable_type'=>'App\User',
        ]);
        App\Image::create([
            'image_type'=> 1,
            'imageable_id'=>1,
            'url' => 'uploads/2WxCFNUCNmBvxrzD0pQqeAMKKp9abLpafZ0aOtSf.jpeg',
            'imageable_type'=>'App\Bano',
        ]);
        
        App\Image::create([
            'image_type'=> 1,
            'imageable_id'=>2,
            'url' => 'uploads/043DwEv19OWBTdduGeZpDIBcrF1gHlDiwPM2lFNJ.jpeg',
            'imageable_type'=>'App\Bano',
        ]);
        App\Image::create([
            'image_type'=> 3,
            'imageable_id'=>2,
            'url' => 'uploads/97zp2XlT4Q1AhpI7c9N3MYAKr9mRzGHpEyd9aXuk.jpeg',
            'imageable_type'=>'App\User',
        ]);
        App\Image::create([
            'image_type'=> 3,
            'imageable_id'=>1,
            'url' => 'uploads/CIPENmdLyM7eAAXIsv2P4qMt1EqXCg2OJ87lVlSK.png',
            'imageable_type'=>'App\User',
        ]);
        

        App\Bano::create([
            'id'=>1,
            'hora_inicio'=>'09:00',
            'hora_fin'=>'19:00',
            'user_id' => 1,
            'jabon' => 1,
            'shampu' => 1,
            'direccion' => 'General bustamante 772',
            'lat'=>'-33.451565',
            'long' => '-70.629735',
            'tiempo_maximo' => '20',
            'costo' => '1000'
        ]);
        App\Bano::create([
            'id'=>2,
            'hora_inicio'=>'09:00',
            'hora_fin'=>'19:00',
            'user_id' => 2,
            'jabon' => 1,
            'shampu' => 1,
            'direccion' => 'General bustamante 772',
            'lat'=>'-33.4500207',
            'long' => '-70.6312858,17',
            'tiempo_maximo' => '5',
            'costo' => '1500'
        ]);
        
        App\Fondo::create([
            'user_id'=>1,
            'numero_transaccion'=>'123124',
            'SaldoActual'=>5000,
            'montoTransferencia' => 5000,
            'tipo_transaccion'=>'deposito',
        ]);
        App\Fondo::create([
            'user_id'=>3,
            'numero_transaccion'=>'123124',
            'SaldoActual'=>5000,
            'montoTransferencia' => 5000,
            'tipo_transaccion'=>'deposito',
        ]);
        App\Config::create([
            'tipo_profit'=>'%',
            'monto_profit'=>10,
            'api_de_pago'=>'flow',
        ]);
            /*
        App\Reserva::create([
            'user_pagador' => 1,
            'user_receptor' =>2,
            'bano_id' =>2,
            'costo' =>1000,
            'confirmacion_pagador' => 0,
            'confirmacion_receptor' => 0,
            'fecha_reserva' =>Carbon::now()->subDays(1),
        ]);
        /*
        App\Reserva::create([
            'user_pagador' => 2,
            'user_receptor' =>1,
            'bano_id' =>1,
            'costo' =>500,
            'confirmacion_pagador' => 0,
            'confirmacion_receptor' => 0,
            'fecha_reserva' =>Carbon::now()->addDays(2),
        ]);
        */
    }
}
