<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFondosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fondos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id')->unsigned();
            $table->text('numero_transaccion')->nullable();
            $table->string('saldoActual');
            $table->string('montoTransferencia')->nullable()->commentary('la cantidad de plata que se esta moviendo');
            $table->string('numero_pago_api')->nullable();
            $table->string('api')->nullable()->commentary('con que api se esta realizando el pago, ej:flow, transbank');
            $table->string('num_orden_sopdar')->nullable();
            $table->boolean('api_transaction_status')->nullable();
            $table->string('monto_ingresado')->nullable()->commentary('El monto que estan ingresando sin impuesto ni comision del API');
            $table->string('token')->nullable();
            $table->text('tipo_transaccion')->commentary('plata adentro o afuera');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fondos');
    }
}
