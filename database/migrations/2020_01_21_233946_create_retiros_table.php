<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRetirosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('retiros', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id')->unsigned();
            $table->string('numero_cuenta');
            $table->string('nombre_banco');
            $table->string('tipo_cuenta');
            $table->string('nombre_titular');
            $table->string('monto');
            $table->string('rut');
            $table->string('numero_referencia');
            $table->string('banco_emisor')->commentary('banco que emite el pago');
            $table->timestamp('transferido')->nullable()->commentary('fecha de cuando se realizo la transferencia');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('retiros');
    }
}
