<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBanosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id')->unsigned();
            $table->integer('tiempo_maximo')->nullable();
            $table->string('long')->nullable();
            $table->string('lat')->nullable();
            $table->string('direccion');
            $table->string('meta_direccion')->nullable();
            $table->text('costo')->nullable();
            $table->boolean('jabon')->default(0);
            $table->boolean('pestillo')->default(0);
            $table->boolean('parking_para_bicicleta')->default(0);
            $table->boolean('parking_para_carro')->default(0);
            $table->boolean('agua_caliente')->default(0);
            $table->boolean('secador_de_pelo')->default(0);
            $table->boolean('enchufe')->default(0);
            $table->boolean('toalla')->default(0);
            $table->boolean('shampu')->default(0);
            $table->boolean('activo')->default(1);
            $table->text('hora_inicio');
            $table->text('hora_fin');
            $table->timestamps();
        });
        /*
        Schema::table('banos', function($table) {
            $table->foreign('user_id')->references('id')->on('users');
        });
        */
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banos');
    }
}

