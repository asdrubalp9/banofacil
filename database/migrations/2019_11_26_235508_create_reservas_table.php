<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReservasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_pagador')->unsigned();//el que usa el bano
            $table->integer('user_receptor')->unsigned();// el que presta el bano
            $table->integer('bano_id')->unsigned();
            $table->text('costo');
            $table->boolean('confirmacion_pagador');
            $table->boolean('confirmacion_receptor');
            $table->timestamp('fecha_reserva');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservas');
    }
}
