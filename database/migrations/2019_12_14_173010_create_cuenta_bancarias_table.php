<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCuentaBancariasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cuenta_bancarias', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id')->unsigned();
            $table->string('nombre_titular');
            $table->string('rut')->commentary('Ingresa 7 u 8 caracteres y escribe el guión antes del código verificador.');
            $table->string('banco');
            $table->string('numero_de_cuenta');
            $table->string('tipo_de_cuenta');
            //$table->string('dia_de_la_semana')->commentary('dia de la semana en la q se le hara la transferencia a la persona');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cuenta_bancarias');
    }
}
