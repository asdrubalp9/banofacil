<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reviews', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('review_id')->nullable();
            $table->unsignedBigInteger('user_emisor');
            $table->unsignedBigInteger('user_receptor');
            $table->integer('reserva_id')->unsigned();
            $table->integer('rating')->commentary('1 de 5');
            $table->text('comentario');
            $table->boolean('visto_receptor')->default(0);
            $table->boolean('activo')->default(1);
            
            $table->timestamps();
        });
        Schema::table('reviews', function (Blueprint $table) {
            $table->foreign('review_id')->references('id')->on('reviews');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reviews');
    }
}
