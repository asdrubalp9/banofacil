<?php

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/banos', 'BanosController@index')->name('banos.index');
Route::get('/banos/crear', 'BanosController@create')->name('banos.crear');
Route::post('/banos/guardar', 'BanosController@store')->name('banos.store');
Route::post('/banos/actualizar/{bano}', 'BanosController@update')->name('banos.update');
Route::post('/banos/foto/actualizar/{bano}','BanosController@updateImgs')->name('banos.updatePics');

Route::get('/banos/buscar', 'BanosController@find')->name('banos.find');
Route::get('/banos/getAllBanos', 'BanosController@getAll')->name('banos.findAll');

Route::get('/banos/reservar/{id}', 'BanosController@contratar')->name('banos.contratar');
Route::post('/banos/reservar/{id}', 'BanosController@reservar')->name('banos.reservar');
Route::post('/banos/usar/{id}', 'BanosController@usar')->name('banos.usar');
Route::post('/banos/prestar/{id}', 'BanosController@prestar')->name('banos.prestar');

Route::get('/reviews/crear/', 'ReviewController@create')->name('reviews.create');
Route::post('/reviews/crear/', 'ReviewController@store')->name('reviews.store');
Route::get('/reviews', 'ReviewController@index')->name('reviews.index');
Route::get('/reviews/{id}', 'ReviewController@visto')->name('reviews.visto');


Route::get('/pagos', 'PagosController@index')->name('pagos.index');
Route::get('/pagos/agregar', 'PagosController@index')->name('pagos.add');
Route::post('/pagos/agregar', 'PagosController@postAdd')->name('pagos.postAdd');
Route::get('/pagos/confirmar', 'PagosController@confirm')->name('pagos.confirm');
Route::post('/pagos/resultado', 'PagosController@result')->name('pagos.result');
Route::get('/test','PagosController@test');

Route::get('/fotos/{foto}','ImagesController@borrar')->name('foto.borrar');
Route::get('/galeria','ImagesController@carousel')->name('foto.carousel');

Route::get('/usuarios/ver/{id?}','UsersController@index')->name('users.index');
Route::get('/usuarios/aprobarUsuario/{id?}/{clave?}','UsersController@aprobarUsuario')->name('users.aprobarUsuario');
Route::post('/usuarios/validar/{id}','UsersController@verify')->name('users.verify');


Route::get('/perfil','UsersController@profile')->name('users.profile');
//Route::get('/perfil','UsersController@update')->name('users.update');
Route::post('/perfil/actualizar','UsersController@update')->name('users.update');
Route::get('/perfil/bancos','BancariasController@index')->name('bancos.index');
Route::post('/perfil/bancos','BancariasController@store')->name('bancos.store');

Route::get('/perfil/sin-verificar','UsersController@unverifiedAccount')->name('unverifiedAccount');
Route::get('/perfil/banned','UsersController@bannedAccount')->name('unverifiedAccount');

Route::get('/transacciones','UsersController@transactionIndex')->name('transacciones');
/*
Route::get('/retiros','RetiroController@index')->name('retiros.index');
Route::post('/retiros','RetiroController@create')->name('retiros.create');
*/
Route::get('/retiros/ver','RetiroController@ver')->name('retiros.ver');
Route::post('/retiros/ver','RetiroController@add')->name('retiros.add');
Route::get('/retiros/historial','RetiroController@historial')->name('retiros.historial');

// */




Route::get('/telegram',function(){
    $opciones = array(
        'http'=>array(
          'method'=>"GET",
          'header'=>"Accept-language: en\r\n" .
                    "Cookie: foo=bar\r\n"
        )
      );
      
    $contexto = stream_context_create($opciones);
    file_get_contents('https://api.telegram.org/bot1003788364:AAEQRO-YOKBTbQXN3Ilj38G9cSKj4aU54L4/sendMessage?chat_id=929814963&text=la gente es muy pendeja', false, $contexto);
});



Route::get('/mailable', function () {
  /*
  $details['name'] = 'jony';
  $details['title'] = 'Su perfil en '.config('app.name', 'Laravel').' no ha podido ser verificado ';
  $details['msg'] = 'la excusa';
  \Mail::to('asdrubalp9@gmail.com')->send(new \App\Mail\Mail($details));
  */
  dd('fin');
  //return new App\Mail\Mail($details);
});