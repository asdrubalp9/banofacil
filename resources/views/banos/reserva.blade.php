@extends('layouts.app')

@section('content')
<script src="{{ asset('js/moment.js') }}" ></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/js/tempusdominus-bootstrap-4.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/css/tempusdominus-bootstrap-4.min.css" />
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    Reservar Ba&ntilde;o
                </div>

                <div class="card-body">
                    <form action="{{route('banos.reservar',[ 'id'=> $bano->id ])}}" method="post">
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-12">
                                            @if( count($imagenes) > 0)
                                            <div id="carousel" class="carousel slide card" data-ride="carousel" >
                                                <div class="carousel-inner" >
                                                
                                                    @foreach($imagenes as $img)
                                                        <div class="carousel-item {{ ($loop->first )? 'active': '' }}" style="max-height:300px">
                                                            <img src="{{url("/storage/")}}/{{ $img}}" class="d-block w-100"  style="max-height:250px">
                                                        </div>
                                                    @endforeach
                                                    <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev"> <span class="carousel-control-prev-icon" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a>
                                                    <a class="carousel-control-next" href="#carousel" role="button" data-slide="next"> <span class="carousel-control-next-icon" aria-hidden="true"></span> <span class="sr-only">Next</span> </a>
                                                </div>
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <b>Costo:</b> {{ $bano->costo}}  pesos<br>
                                <b>Genero de la persona que arrienda:</b> {{ $data->genero }}  <br>
                                <b>Promedio de calificaciones:</b> {{ $data->cantCalificaciones }}<br>
                                <b>Tiene agua caliente:</b> {{ $bano->agua_caliente}} <br>
                                <b>Suministra jabon:</b> {{ $bano->jabon}} <br>
                                <b>Suministra shampu:</b> {{ $bano->shampu}} <br>
                                <b>Tiene enchufe:</b> {{ $bano->enchufe}} <br>
                                <b>Tiene estacionamiento para bicicleta:</b> {{ $bano->parking_para_bicicleta}} <br>
                                <b>Tiene estacionamiento para carro:</b> {{ $bano->parking_para_carro}} <br>
                                <b>La puerta cuenta con pestillo:</b> {{ $bano->pestillo}} <br>
                                <b>Hay secador de pelo:</b> {{ $bano->secador_de_pelo}} <br>
                                <b>Tiempo m&aacute;ximo de ducha:</b> {{ $bano->tiempo_maximo}} mins <br>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Fecha en el que lo vas a usar</label>
                            <input name="fecha" type="date"  class="form-control @error('fecha') is-invalid @enderror"  placeholder="Fecha en el que lo vas a usar" value="{{ old('fecha') }}">
                            @error('fecha')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <!-- 
<div class="container">
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <div class="input-group date" id="datetimepicker3" data-target-input="nearest">
                    <input type="text" class="form-control datetimepicker-input" data-target="#datetimepicker3"/>
                    <div class="input-group-append" data-target="#datetimepicker3" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-clock-o"></i></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
-->


                        <div class="form-group">
                            <label>Hora en el que lo vas a usar</label>
                            <input name="hora" type="time" min="{{$bano->hora_inicio}}" max="{{$bano->hora_fin}}" class="form-control @error('fecha') is-invalid @enderror"  placeholder="Hora en el que lo vas a usar" value="{{ old('hora') }}">
                            @error('hora')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <button class="mx-auto btn btn-success" name="opc">
                            Reservar
                        </button>
                        <a href="{{ route('banos.find') }}" class="mx-auto btn btn-danger">
                            Elegir de nuevo
                        </a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function () {
        $('.btn-success').on('click',function(e){
            e.preventDefault();
            Swal.fire({
                title: '¿Esta seguro desea reservar este baño?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si'
            }).then((result) => {
                if (result.value) {
                    $('form').submit();
                }
            })

        })
    });
</script>

@endsection