<div class="">
    <h3>Subir fotos</h3>
    <small class="text-muted">Hasta 5 imágenes</small>
        <div class="fotoHolder">
            <div class="listFoto">
                <div class="p-2">
                    <input type="file" name="fotos[]" >
                </div>
            </div>
        </div>  
    <button type="button" class="btn btn-success addFoto mx-2" >
        <i class="fas fa-plus-circle"></i> Foto adicional
    </button>
</div>