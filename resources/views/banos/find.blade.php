@extends('layouts.app')

@section('content')
<div class="container-fluid">

<!-- <div class=" shadow-sm" style="position: absolute;z-index: 1; width: 100%; background-color: #ffffff;">
    <label class="text-center d-block">
        Seleccione el tipo de contrato
    </label>
        <select class="form-control SelectTipoContrato" id="type" onchange="filterMarkers();">
            <option value="">Todos los tipos de contrato</option>            
        </select>
</div> -->
    <div class="row" id="map"  class="z-depth-1-half map-container" style="height:90vh">
    </div>
</div>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCCK_rzXLZJDWf2TKkYC7zBwzamZC3yRP0"  ></script>
<script>

    $(document).ready(function(){
        !function(n,e,o){function s(n,e){return typeof n===e}function a(){var n,e,o,a,i,f,r;for(var c in l)if(l.hasOwnProperty(c)){if(n=[],e=l[c],e.name&&(n.push(e.name.toLowerCase()),e.options&&e.options.aliases&&e.options.aliases.length))for(o=0;o<e.options.aliases.length;o++)n.push(e.options.aliases[o].toLowerCase());for(a=s(e.fn,"function")?e.fn():e.fn,i=0;i<n.length;i++)f=n[i],r=f.split("."),1===r.length?Modernizr[r[0]]=a:(!Modernizr[r[0]]||Modernizr[r[0]]instanceof Boolean||(Modernizr[r[0]]=new Boolean(Modernizr[r[0]])),Modernizr[r[0]][r[1]]=a),t.push((a?"":"no-")+r.join("-"))}}function i(n){var e=r.className,o=Modernizr._config.classPrefix||"";if(c&&(e=e.baseVal),Modernizr._config.enableJSClass){var s=new RegExp("(^|\\s)"+o+"no-js(\\s|$)");e=e.replace(s,"$1"+o+"js$2")}Modernizr._config.enableClasses&&(e+=" "+o+n.join(" "+o),c?r.className.baseVal=e:r.className=e)}var t=[],l=[],f={_version:"3.5.0",_config:{classPrefix:"",enableClasses:!0,enableJSClass:!0,usePrefixes:!0},_q:[],on:function(n,e){var o=this;setTimeout(function(){e(o[n])},0)},addTest:function(n,e,o){l.push({name:n,fn:e,options:o})},addAsyncTest:function(n){l.push({name:null,fn:n})}},Modernizr=function(){};Modernizr.prototype=f,Modernizr=new Modernizr;var r=e.documentElement,c="svg"===r.nodeName.toLowerCase();Modernizr.addTest("geolocation","geolocation"in navigator),a(),i(t),delete f.addTest,delete f.addAsyncTest;for(var u=0;u<Modernizr._q.length;u++)Modernizr._q[u]();n.Modernizr=Modernizr}(window,document);
        initMap();        
        var map;
        var markers = [];
        var baseUrl = '{{ url('/')}}';


        filterMarkers = function () {
                getPoints().then(function(info){
                    setMapOnAll(null);
                    
                    filter = $("#type")[0].value;
                    if(filter)
                    {
                        filterPoints = info.filter(a=>a.TipoContrato==filter);
                    }
                    
                    filterPoints.map(function( val) {
                        console.log(val);
                        let locationData = {
                            position: {lat: parseFloat(val.lat), lng: parseFloat(val.long)},
                            name: 'val.nombre',
                            animation: google.maps.Animation.DROP,
                            icon: '{{ url('/') }}images/icon2.png'
                        };
                    createPoints(info)
                                                    
                    var marker = new google.maps.Marker({position: locationData.position, name:'name', map: map , icon: locationData.icon});
                    
                    marker.addListener('click', function() {
                        //infowindow.close();
                        /*
                        $('#idCorreo').val(infowindow.userId);
                        $('#dataInfo').html(infowindow.content);
                        $('#modal').modal('show');
                        // */
                        //infowindow.open(map, marker);
                    });
                        markers.push(marker);
                    });
                });
        }

    function getPoints(){
        
        return $.ajax({
            type: 'GET',
            url: '{{route('banos.findAll')}}',
            dataType: "json",
            success: function(msg){
                console.log(msg)
                return msg;
            },
            beforeSend: function() {
                $('Body').append('<div class="blocker"><i class="fas fa-sync fa-spin"></i></div>').fadeIn();
            },
            complete: function () {
                $('.blocker').fadeOut();

            }
        });
    }

        
        function initMap() {
            
            $('#contactPerson').on('click',function(){

                var params={
                    'idUsuario'     : $('#idCorreo').val(),
                    'mensaje'       : $('#mensaje').val(),
                    'captcha'       : grecaptcha.getResponse(),
                }
                
                Swal.fire({
                    title: 'Esta seguro desea enviarle un mensaje a este usuario?',
                    showCancelButton: true,
                    showConfirmButton:true,
                    confirmButtonText:
                        '<i class="fa fa-thumbs-up"></i> SI!',
                    cancelButtonText:
                        '<i class="fa fa-thumbs-down"></i> NO',
                }).then(function(data){
                    if(data.dismiss != 'cancel'){
                        sendEmail(params)
                            .then(function(info){
                                
                                Swal.fire({
                                    type: info.type,
                                    html:info.msg
                                });
                                if(info.type == 'success'){
                                    $('#form').trigger("reset");
                                }
                                
                            });
                        grecaptcha.reset();
                    }
                });
            })

    getPoints().then(function(info){
        var styles = {
                hide: [
                    {
                        featureType: 'poi',
                        stylers: [{visibility: 'off'}]
                    },
                    {
                        featureType: 'transit',
                        elementType: 'labels.icon',
                        stylers: [{visibility: 'off'}]
                    }
                ]
            };
        createPoints(info);
        map.setOptions({styles: styles['hide']});
        var contentString = new Object();
    }); 

    function createPoints(info){
        
        let logged = info.userData
        //console.log('logged', logged)
        //console.log(info.Points)    
        /*
        var value = $.ajax({
                type: 'GET',
                url: '{{route('foto.carousel')}}',
                success: function(msg){
                    console.log('ajax')
                    //console.log(msg)
                    //return msg;
            }});
            console.log(value)
            */
        $.each(info.Points,function(i, val) {
            
                if(val.img){
                    var galeria = '<div><h4 class="text-center">fotos</h4><div class="container-fluid"><div class="row"> <div class="col-md-12"> <div id="carousel" class="carousel slide card" data-ride="carousel"> <div class="carousel-inner"> <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev"> <span class="carousel-control-prev-icon" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a> <a class="carousel-control-next" href="#carousel" role="button" data-slide="next"> <span class="carousel-control-next-icon" aria-hidden="true"></span> <span class="sr-only">Next</span> </a> </div></div></div></div></div>';
                    galeria = $.parseHTML( galeria )
                    var i = 0
                    //console.log(galeria)
                    $.each(val.img,function(i, value) {
                        active = (i == 0)?'active':'';
                        let img = '<div class="carousel-item '+active+'" style="max-height:300px"><img src={{url("/storage/")}}/'+value+' class="d-block w-100"  style="max-height:250px"> </div>';
                        
                        $( galeria).find('.carousel-inner').append( $.parseHTML(img) )
                        i++;
                    });
                    
                    galeria = $(galeria).html()
                }else{
                    var galeria = '';
                }
                
                let locationData = {
                    position:{ lat:parseFloat(val.data.lat) , lng:parseFloat(val.data.long) },
                    icon: '{{ url('/') }}/images/icon2.png',
                    animation: google.maps.Animation.DROP,
                };
                let cal = (val.data.cantCalificaciones>0)? 'calificación': 'calificaciones';
                cal = (val.data.promCalificaciones)? val.data.promCalificaciones +' de '+val.data.cantCalificaciones +' '+cal :'No posee calificaciones.' ;
                var contentString =
                    '<div id="bodyContent">'+
                        '<b>Genero de la persona que arrienda:</b> '+val.data.genero +' <br>'+
                        '<b>Promedio de calificaciones:</b> '+cal+'<br>'+
                        '<b>Costo:</b> '+val.data.costo +' pesos<br>'+
                        '<b>Tiene agua caliente:</b> '+val.data.agua_caliente +'<br>'+
                        '<b>Suministra jabon:</b> '+val.data.jabon +'<br>'+
                        '<b>Suministra shampu:</b> '+val.data.shampu +'<br>'+
                        '<b>Tiene enchufe:</b> '+val.data.enchufe +'<br>'+
                        '<b>Tiene estacionamiento para bicicleta:</b> '+val.data.parking_para_bicicleta +'<br>'+
                        '<b>Tiene estacionamiento para carro:</b> '+val.data.parking_para_carro +'<br>'+
                        '<b>La puerta cuenta con pestillo:</b> '+val.data.pestillo +'<br>'+
                        '<b>Hay secador de pelo:</b> '+val.data.secador_de_pelo +'<br>'+
                        '<b>Tiempo m&aacute;ximo de ducha:</b> '+val.data.tiempo_maximo +'<br>'+
                        galeria+
                        '<p class="text-center"><a href="' + baseUrl + '/banos/reservar/'+val.data.id+'" class="btn btn-success d-block mx-auto my-2">Reservar</a>'+
                    '</div>';
                
                var infowindow = new google.maps.InfoWindow({
                    content: contentString,
                    userId : val.UserId
                });

                var marker = new google.maps.Marker({position: locationData.position, name:name, map: map , icon: locationData.icon});
                
                marker.addListener('click', function() {
                    //infowindow.close();
                    $('#dataInfo').html(infowindow.content);
                    $('#modal').modal('show');
                });
                                
                
                markers.push(marker);
                return markers;
            
        });
    } 
            
            if (Modernizr.geolocation) {
                navigator.permissions.query({name:'geolocation'}).then(function(result) {
                    if (result.state == 'granted') {
                        
                        navigator.geolocation.getCurrentPosition (function(position) {
                            map = new google.maps.Map(document.getElementById('map'), {
                                center: {lat: position.coords.latitude, lng: position.coords.longitude},
                                zoom: 16,
                                streetViewControl: false,mapTypeControl: false
                            });
                        });
                    } else if (result.state == 'prompt') {
                        lat=-33.452281;
                        long = -70.628448;
                        zoom = 14;
                        //navigator.geolocation.getCurrentPosition(revealPosition,positionDenied,geoSettings);
                    } else if (result.state == 'denied') {
                        lat=-33.452281;
                        long = -70.628448;
                        zoom = 14;
                    }
                        map = new google.maps.Map(document.getElementById('map'), {
                            center: {lat: -33.452281, lng: -70.628448},
                            zoom: 14,
                            streetViewControl: false,mapTypeControl: false
                        });
                    
                    });   
            }
        }
    }) //end document ready
</script>

<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
  <div class="modal-dialog " role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalLabel">
            Informaci&oacute;n del ba&ntilde;o
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form>
            <div class="modal-body">
                <div id="dataInfo">
                </div>
            </div>
        </form>
    </div>
  </div>
</div>
@endsection
