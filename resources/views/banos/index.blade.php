@extends('layouts.app')

@section('content')
<div class="container py-4">
    <div class="row justify-content-center">
        <div class="col-md-11">
            <div class="card">
                <div class="card-header">
                    Panel de control
                    <a href="{{ route('banos.crear') }}" class="btn btn-success">
                        Registrar ba&ntilde;o
                    </a>
                    <a href="{{ route('transacciones') }}" class="btn btn-info float-right">
                        <i class="fas fa-wallet"></i>
                            {{ $saldoActual }} 
                    </a>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <h3>
                                Mis ba&ntilde;os disponibles
                            </h3>
                        </div>
                        @foreach( $banos as $bano)
                            <div class=" col-md-6 ">
                                <div class="card ">
                                    <form class="card-body" action="{{ route('banos.update', ['bano'=> $bano->id ] )}}" method="post"  autocomplete="off">
                                        @csrf
                                        @include('banos.form',['type'=> 'update'])
                                        <button class="btn btn-success">
                                            Actualizar
                                        </button>
                                    </form>
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="col-md-12">
                                                @if( count($bano->image) > 0)
                                                    @include('banos.images',['fotos'=> $bano->image, 'id'=>$bano->id ])
                                                @endif
                                            </div>
                                        </div>
                                        <div class="row">
                                            <form class="col-md-12" action="{{ route('banos.updatePics', ['bano'=> $bano->id ] )}}" method="post" enctype="multipart/form-data">
                                                <div class="card mb-3">
                                                    <div class="card-body">
                                                        @csrf
                                                        @include('banos.imageForm')
                                                        <div class="row">
                                                            <div class="col-md-4 my-2 mx-auto">
                                                                <button class="btn btn-success">
                                                                    Subir fotos
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('partials.imageScript')
@endsection
