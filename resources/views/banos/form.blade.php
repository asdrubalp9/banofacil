<div class="form-group">

    <label>Costo por uso</label>
    <input name="costo" type="number" min="0" class="form-control @error('costo') is-invalid @enderror" aria-describedby="Costo por uso" placeholder="Cuanto cobrar&iacute;a por el uso del ba&ntilde;o" value="{{ old('costo',$bano->costo) }}">
    @error('costo')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror
    
    <label>Tiempo m&aacute;ximo de uso en minutos <small class="text-muted"> | solo n&uacute;meros, ejemplo 15 para 15 mins. </small></label>
    <input name="tiempo_maximo" type="number" min="0" class="form-control @error('tiempo_maximo') is-invalid @enderror"  placeholder="Tiempo m&aacute;ximo de uso en minutos " value="{{ old('tiempo_maximo',$bano->tiempo_maximo) }}">
    @error('tiempo_maximo')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror
    <label>Hora de inicio en la que el ba&ntildeo empieza a estar disponible</label>
    <input name="hora_inicio" type="time"  class="form-control @error('hora_inicio') is-invalid @enderror"  placeholder="Tiempo m&aacute;ximo de uso en minutos " value="{{ old('hora_inicio',$bano->hora_inicio) }}">
    @error('hora_inicio')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror

    <label>Hora fin en la que el ba&ntildeo deja de estar disponible</label>
    <input name="hora_fin" type="time"  class="form-control @error('hora_fin') is-invalid @enderror"  placeholder="Tiempo m&aacute;ximo de uso en minutos " value="{{ old('hora_fin',$bano->hora_fin) }}">
    @error('hora_fin')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror

    <label>Direcci&oacute;n <small class="text-muted">Nombre de la calle, el n&uacute;mero de la casa, comuna, y al final elija uno de la lista emergente.</small></label>
    <p id="opc" class="text-danger d-none" >
        seleccione una de las opciones desplegadas para completar la direcci&oacute;n
    </p>
    <input id="Direccion" name="direccion" type="text" class="form-control @error('direccion') is-invalid @enderror" placeholder="Direcci&oacute;n del baño"  value="{{ old('direccion',$bano->direccion) }}" autocomplete="off">
    @error('direccion')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror
    <input type="hidden"  id="lat" name="lat" placeholder="LAT" >
    <input type="hidden"  id="long" name="long" placeholder="LONG" >

    
    <label>Datos adicionales de la direcci&oacute;n <small class="text-muted">Apartamento | n&uacute;mero de casa | bloque.</small></label>
    <input name="meta_direccion" type="text" class="form-control @error('meta_direccion') is-invalid @enderror" placeholder="Datos adicionales de la direcci&oacute;n | Apartamento | n&uacute;mero de casa | bloque."  value="{{ old('meta_direccion',$bano->meta_direccion) }}">
    @error('meta_direccion')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror
    
    
    @if( isset($type) && $type == 'create' )
        <div style="width:100%; height:400px;" id="map"></div>

    @endif
    

    <div class="custom-control custom-switch">
        <input name="jabon" type="checkbox" class="custom-control-input" id="jabon{{$bano->id}}" {{ ( old('jabon') == 'on' || $bano->jabon == 'Sí' )? 'checked' : ''  }}>
        <label class="custom-control-label" for="jabon{{$bano->id}}">
            Proporciona jab&oacute;n?
        </label>
    </div>
    @error('jabon')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror
    <div class="custom-control custom-switch">
        <input name="shampu" type="checkbox" class="custom-control-input" id="shampu{{$bano->id}}" {{ ( old('shampu') == 'on'  || $bano->shampu == 'Sí' )? 'checked' : ''  }}>
        <label class="custom-control-label" for="shampu{{$bano->id}}">
            proporciona shampoo?
        </label>
    </div>
    @error('shampu')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror
    <div class="custom-control custom-switch">
        <input name="pestillo" type="checkbox" class="custom-control-input" id="pestillo{{$bano->id}}" {{ ( old('pestillo')  == 'on' || $bano->pestillo == 'Sí' )? 'checked' : ''  }}>
        <label class="custom-control-label" for="pestillo{{$bano->id}}">
            La puerta tiene pestillo?
        </label>
    </div>
    @error('pestillo')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror
    
    <div class="custom-control custom-switch">
        <input name="parking_para_bicicleta" type="checkbox" class="custom-control-input" id="parking_para_bicicleta{{$bano->id}}" {{ ( old('parking_para_bicicleta') == 'on' || $bano->parking_para_bicicleta == 'Sí' )? 'checked' : ''  }}>
        <label class="custom-control-label" for="parking_para_bicicleta{{$bano->id}}">
            Hay disponibilidad para guardar una bicicleta?
        </label>
    </div>
    @error('parking_para_bicicleta')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror

    <div class="custom-control custom-switch">
        <input name="parking_para_carro" type="checkbox" class="custom-control-input" id="parking_para_carro{{$bano->id}}" {{ ( old('parking_para_carro') == 'on' || $bano->parking_para_carro == 'Sí' )? 'checked' : ''  }}>
        <label class="custom-control-label" for="parking_para_carro{{$bano->id}}">
            Hay posibilidad de estacionar un carro de manera segura?
        </label>
    </div>
    @error('parking_para_carro')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror
    <div class="custom-control custom-switch">
        <input name="agua_caliente" type="checkbox" class="custom-control-input" id="agua_caliente{{$bano->id}}" {{ ( old('agua_caliente') == 'on' || $bano->agua_caliente == 'Sí' )? 'checked' : ''  }}>
        <label class="custom-control-label" for="agua_caliente{{$bano->id}}">
            El ba&ntilde;o cuenta con agua caliente?
        </label>
    </div>
    @error('agua_caliente')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror

    <div class="custom-control custom-switch">
        <input name="secador_de_pelo" type="checkbox" class="custom-control-input" id="secador_de_pelo{{$bano->id}}" {{ ( old('secador_de_pelo') == 'on' || $bano->secador_de_pelo == 'Sí' )? 'checked' : ''  }}>
        <label class="custom-control-label" for="secador_de_pelo{{$bano->id}}">
            Proporcionar&aacute; secador de pelo?
        </label>
    </div>
    @error('secador_de_pelo')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror

    <div class="custom-control custom-switch">
        <input name="enchufe" type="checkbox" class="custom-control-input" id="enchufe{{$bano->id}}" {{ ( old('enchufe')  == 'on' || $bano->enchufe == 'Sí' )? 'checked' : ''  }}>
        <label class="custom-control-label" for="enchufe{{$bano->id}}">
            Hay enchufe como para conectar un cargador de tel&eacute;fono o secador de pelo?
        </label>
    </div>
    @error('enchufe')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror

    <div class="custom-control custom-switch">
        <input name="toalla" type="checkbox" class="custom-control-input" id="toalla{{$bano->id}}" {{ ( old('toalla') == 'on'  || $bano->toalla == 'Sí' )? 'checked' : ''  }}>
        <label class="custom-control-label" for="toalla{{$bano->id}}">
            Ofrece toalla?
        </label>
    </div>
    @error('toalla')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror
    
    
</div>
<script>


    //initMap();


function initMap() {
  getGeo();
    var lat = $('#lat').val();
    var long = $('#long').val();
    var zoomy = 17;
    
    if( lat == '' && long == '' ){
      lat = -33.452250;
      long = -70.638173;
      zoomy = 13;
    }
    var styles = {
              hide: [
                  {
                      featureType: 'poi',
                      stylers: [{visibility: 'off'}]
                  },
                  {
                      featureType: 'transit',
                      elementType: 'labels.icon',
                      stylers: [{visibility: 'off'}]
                  }
              ]
          };
          
  var map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: lat, lng: long},
    zoom: zoomy,
  });
  map.setOptions({styles: styles['hide']});
  var card = document.getElementById('pac-card');
  var input = document.getElementById('Direccion');
  
  var autocomplete = new google.maps.places.Autocomplete(input);
  autocomplete.bindTo('bounds', map);
  autocomplete.setFields(
      ['address_components', 'geometry', 'icon', 'name']);
  var marker = new google.maps.Marker({
    map: map,
    anchorPoint: new google.maps.Point(0, -29)
  });

  autocomplete.addListener('place_changed', function() {
    marker.setVisible(false);
    var place = autocomplete.getPlace();
    if (!place.geometry) {
      $("#Direccion").addClass("is-invalid");
      $("#opc").removeClass("d-none");
      return;
    }else{
      $("#Direccion").addClass("is-valid");
          if (place.geometry.viewport) {
              map.fitBounds(place.geometry.viewport);
          } else {
              map.setCenter(place.geometry.location);
              map.setZoom(17);
          }
          marker.setPosition(place.geometry.location);
          marker.setVisible(true);
          var address = '';
      if (place.address_components) {
          address = [
          (place.address_components[0] && place.address_components[0].short_name || ''),
          (place.address_components[1] && place.address_components[1].short_name || ''),
          (place.address_components[2] && place.address_components[2].short_name || '')
          ].join(' ');
      }
          $('#lat').val(place.geometry.location.lat());
          $('#long').val(place.geometry.location.lng());
    }
    
  });


}
</script>