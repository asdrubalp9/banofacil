<div class="row my-3">
    <div class="col-md-12">
        <div id="carousel{{ $id }}" class="carousel slide card" data-ride="carousel">
            <div class="carousel-inner">
                @php
                    $i = 0;
                @endphp
                @foreach( $fotos as $foto )
                    <div class="carousel-item  {{ ($i == 0)?'active':'' }} "  style="max-height:250px">
                        <a href="{{ route('foto.borrar',['foto'=>$foto->id]) }}" class="btn btn-danger m-2  col-md-3 position-absolute">
                            Eliminar foto
                        </a>
                        <img src="{{  asset('storage/'.$foto->url) }}" class="d-block w-100"  style="max-height:300px">
                        
                    </div>
                @php
                    $i++;
                @endphp
                @endforeach
            <a class="carousel-control-prev" href="#carousel{{ $id }}" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carousel{{ $id }}" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
            </div>
        </div>
    </div>
</div>