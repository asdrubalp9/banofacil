@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Nombres y apellidos</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus placeholder="Nombres y apellidos">

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="E-mail">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="telefono" class="col-md-4 col-form-label text-md-right">
                                Tel&eacute;fono <small class="text-muted">| Preferiblemente con whatsapp, colocar con codigo de area, ejemplo: 56912345678</small>
                            </label>

                            <div class="col-md-6">
                                <input id="telefono" type="number" min="0" class="form-control @error('telefono') is-invalid @enderror" name="telefono" value="{{ old('telefono') }}" required placeholder="Tel&eacute;fono | Preferiblemente con whatsapp, colocar con codigo de area, ejemplo: 56912345678" >

                                @error('telefono')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="rut" class="col-md-4 col-form-label text-md-right">
                                RUT: <small class="text-muted">| Su numero de RUT con guión y digito de verificación, ejemplo: 12345678-K</small>
                            </label>

                            <div class="col-md-6">
                                <input id="rut" type="text" class="form-control @error('rut') is-invalid @enderror" name="rut" value="{{ old('rut') }}" required placeholder="| Su numero de RUT con guión y digito de verificación, ejemplo: 12345678-K" >

                                @error('rut')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="genero" class="col-md-4 col-form-label text-md-right">Genero</label>

                            <div class="col-md-6">
                            
                            <select name="genero"  class="form-control @error('genero') is-invalid @enderror" required>
                                @if(old('genero' ))
                                    <option value="{{old('genero' )}}">
                                        {{old('genero' )}}
                                    </option>
                                @endif
                                <option value="Masculino">
                                    Masculino
                                </option>
                                <option value="Femenino">
                                    Femenino
                                </option>
                                <option value="Otro">
                                    Otro
                                </option>
                            </select>
                                @error('genero')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">
                            {{ __('Password') }}
                            <span>| Al menos 8 caracteres</span></label>
                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="Contraseña">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" placeholder="Confirmar contraseña">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="imagen" class="col-md-4 col-form-label text-md-right">
                                Foto para validar tu perfil |
                                <small class="muted">
                                    Para poder garantizar la seguridad de los usuarios, necesitamos que te tomes una fotografia con tu carnet vigente para poder validar tu persona, esta foto solo la veremos nosotros.
                                </small>
                                @error('verificacion')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </label>
                            <input type="file" name="verificacion" >
                        </div>
                        <div class="form-group row">
                            <label for="imagen" class="col-md-4 col-form-label text-md-right">
                                Foto de usuario | 
                                <small class="muted">
                                    Si deseas, puedes colocar una foto tuya para que los usuarios te vean.
                                </small>
                            </label>
                            <input type="file" name="avatar" >
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
