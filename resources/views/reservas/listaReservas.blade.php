<ul class="list-group ">
    <li class="list-group-item">
        <div class="row">
            @php
                $img = $transaccion->usuario->image()->where('image_type','=',3)->first();
                
                    if($img){
                        echo '<div class="col-md-6">';
            @endphp
                                <img class="avatar img-thumbnail rounded-circle" src="{{ url('/storage/').'/'.$img->url }}" style="max-width:130px; height:130px">
            @php
                        echo '</div>';
                    }
            @endphp

            <div class="{{ ( $img )?'col-md-6':'col-md-12' }} pt-4">
                {{ $transaccion->usuario->name }}<br>
            
                Genero: {{ $transaccion->usuario->genero }}<br>

                Tel&eacute;fono: {{ $transaccion->usuario->telefono }}
            </div>
        </div>
    </li>
    <li class="list-group-item">
        Fecha de la reserva: {{ $transaccion->fecha_reserva->format('d-m-Y G:i') }}
    </li>
    <li class="list-group-item">
       Costo: {{ number_format($transaccion->costo,0,'','.') }} CLP
    </li>
    
    <li class="list-group-item">
        Baño a usar: {{ $transaccion->bano->direccion }}
    </li>
    @if($transaccion->confirmacionReceptor)
        <li class="list-group-item">       
            {{ $transaccion->usado }}
        </li>
    @endif
    <li class="list-group-item">       
        <div class="row">
            <div class="col-md-6 mx-auto my-2">
                <button name="opc" class="btn btn-block btn-success" name="usado" value="{{ $transaccion->id }}">
                    Ya lo us&eacute;
                </button>
            </div>
        </div>
    </li>
</ul>