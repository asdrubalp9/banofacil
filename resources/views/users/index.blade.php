@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">
                    Validacion de usuarios                    
                </div>
                <div class="card-body">
                    <ul class="list-group">
                        @foreach($users as $user)
                            <li class="list-group-item">
                                <form action="{{route('users.verify',['id'=>$user->id] ) }}" method="post"  autocomplete="off" enctype="multipart/form-data">
                                    @csrf
                                    
                                    Nombre: {{ $user->name }}
                                    <br>
                                    @if($user->telefono)
                                        Telefono: <a href="https://wa.me{{ $user->telefono }}">{{$user->telefono}}</a>
                                    @endif
                                    @if($user->image)
                                        @foreach($user->image as $img )
                                            @if($img->image_type == 2)
                                            <br>
                                            foto de verificacion:
                                                <img class="img img-thumbnail mb-2" style="width:100px; height;100px" src="{{ url("/storage/").'/'.$img->url }}">
                                            @endif
                                            @if($img->image_type == 3)
                                            <br>
                                            foto avatar:
                                                <img class="img img-thumbnail " style="width:100px; height;100px" src="{{ url("/storage/").'/'.$img->url }}">
                                            @endif
                                        @endforeach
                                    @endif
                                    <div class="form-group mt-2">
                                        <input type="text" name="excusa" class="form-control" placeholder="excusa para banearlo" >
                                    </div>
                                    
                                    <button name="opc" value="0" class="btn btn-success">
                                        Aprobar
                                    </button>
                                    <button name="opc" value="1" class="btn btn-danger">
                                        BAN
                                    </button>
                                </form>
                            </li>
  
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>


<div id="modal" class="modal " tabindex="-1" role="dialog">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-body text-center">
      </div>
    </div>
  </div>
</div>

<script>
$(document).ready(function(){
    $('.img').on('click', function(){
        var img = $(this).attr('src')
        console.log(img)
        let str = '<img src="'+img+'" >';
        $('.modal-body').html('')
        $('.modal-body').append(str)
        $('#modal').modal('show')
    })
})
</script>
@endsection
