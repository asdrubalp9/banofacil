@extends('layouts.app') @section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">
                    Mi perfil
                </div>
                <div class="card-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12 col-lg-6  p-3  col-sm-12">
                                <h3 class="text-center">
                                    HOLA!, {{ $user->name }} 
                                </h3>
                                <div class="row">
                                    <div class="col-md-4 col-sm-12">
                                    @if( isset($avatar->url))
                                        <img class="avatar img-thumbnail rounded-circle" src="{{ url('/storage/').'/'.$avatar->url }}" style="max-width:130px; height:130px">
                                    @endif
                                    </div>
                                    <div class="col-md-6">
                                        <p class="text-left ">
                                            {{ __('E-Mail Address') }}: {{ $user->email }}
                                        </p>
                                        
                                        <p class="text-left ">
                                            Genero: {{ $user->genero }}
                                        </p>
                                        <form action="{{route('users.update') }}" method="post" autocomplete="off" enctype="multipart/form-data">
                                            @csrf
                                            <p class="text-left ">
                                                Tel&eacute;fono
                                                <div class="form-group">
                                                    <input type="number" class="form-control @error('telefono') is-invalid @enderror" name="telefono" required value="{{ $user->telefono }}" placeholder="Tel&eacute;fono">
                                                    @error('telefono')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span> @enderror
                                                </div>
                                            </p>
                                            <button type="submit" name="opc" value="telefono" class="btn btn-success mt-2">
                                                Actualizar tel&eacute;fono
                                            </button>
                                        </form>
                                        <form action="{{route('users.update') }}" method="post" autocomplete="off" enctype="multipart/form-data">
                                            @csrf
                                            <label>
                                                Actualizar avatar 
                                                <small class="text-muted">
                                                    Actualiza la foto que los usuarios van a ver
                                                </small>
                                            </label>
                                            <input type="file" name="avatar" >
                                            <button type="submit" name="opc" value="avatar" class="btn btn-success mt-2">
                                                Actualizar avatar
                                            </button>
                                        </form>
                                    </div>
                                </div>
                                </div>
                                <!-- ///////////// -->
                                <div class="col-md-12 col-lg-6 p-3 ">
                                    <h3 class="text-center">
                                        Actualiza tus datos
                                    </h3>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <form action="{{route('users.update') }}" method="post" autocomplete="off" enctype="multipart/form-data">
                                                @csrf
                                                <label for="password" class="">
                                                    {{ __('Password') }} actual
                                                </label>
                                                <div class="form-group">
                                                    <input id="password" type="password" class="form-control" name="current_password" required placeholder="Contraseña actual">
                                                    
                                                </div>
                                                <div class="form-group ">
                                                    <label for="password" class="">
                                                        {{ __('Password') }} nueva
                                                        <small class="text-muted">
                                                            | Al menos 8 caracteres
                                                        </small>
                                                    </label>
                                                    <div class="">
                                                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="Contraseña"> @error('password')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span> @enderror
                                                    </div>
                                                </div>

                                                <div class="form-group ">
                                                    <label for="password-confirm" class="">{{ __('Confirm Password') }}</label>
                                                    <div class="">
                                                        <input id="password-confirm" type="password" class="form-control " name="password_confirmation" required autocomplete="new-password" placeholder="Confirmar contraseña">
                                                        @error('password')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>

                                                <button type="submit" name="opc" value="password" class="btn btn-success mt-2">
                                                    Actualizar
                                                </button>
                                            </form>
                                        </div>
                                    </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
