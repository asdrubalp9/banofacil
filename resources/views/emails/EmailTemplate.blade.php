<html>
<head>
    <title>{{config('app.name', 'Laravel')}}</title>
</head>
<body>
    <h1>{{ $title }}</h1>
    <p>{!! $msg !!}</p>
</body>
</html>