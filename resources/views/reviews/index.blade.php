@extends('layouts.app')

@section('content')
<div class="container py-4">
    <div class="row justify-content-center">
        <div class="col-md-11">
            <div class="card">
                <div class="card-header">
                    Calificaciones
                </div>
                <div class="card-body">
                    <div class="row">
                        
                        <div class="card-body">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-md-6">
                                    <h3>
                                        Calificaciones recibidas por prestar el ba&ntilde;o
                                    </h3>
                                    @if( count($calificacionesPorUsar) > 0 )
                                        @foreach($calificacionesPorUsar as $calificaciones)
                                                <div class="card col-md-12 m-2 my-3">
                                                    <div class="card-body">
                                                    <div class="row">
                                                        <h4 class="col-md-6">
                                                            {{ $calificaciones->name }}
                                                        </h4>
                                                        <h4 class="col text-right">
                                                        {{ $calificaciones->rating }}/5
                                                        
                                                        @if($calificaciones->visto_receptor == 0)
                                                            <a href="{{route('reviews.visto',['id' => $calificaciones->id ] ) }}" class="btn btn-info">
                                                                <i class="far fa-eye"></i>
                                                            </a>
                                                        @endif
                                                        </h4>
                                                    </div>
                                                    <div class="d-block">
                                                        <p>
                                                            <b>
                                                            comentario:<br>
                                                            </b>
                                                            {{ $calificaciones->comentario }}
                                                        </p>
                                                        <small class="muted float-right">{{ date( 'd-m-Y G:i', strtotime($calificaciones->created_at)) }}</small>
                                                    </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        @else
                                            <h4>No lo han calificado todav&iacute;a</h4>
                                        @endif
                                    </div>
                                    <div class="col-md-6">
                                        <h3>
                                            Calificaciones recibidas por usar los ba&ntilde;os
                                        </h3>
                                            @if( count($calificacionesPorPrestar) > 0 )
                                                @foreach($calificacionesPorPrestar as $calificaciones)
                                                    <div class="card col-md-12 m-2 my-3">
                                                        <div class="card-body">
                                                        <div class="row">
                                                            <h4 class="col-md-6">
                                                                {{ $calificaciones->name }}
                                                            </h4>
                                                            <h4 class="col text-right">
                                                            {{ $calificaciones->rating }}/5
                                                            
                                                            @if($calificaciones->visto_receptor == 0)
                                                                <a href="{{route('reviews.visto',['id' => $calificaciones->id ] ) }}" class="btn btn-info">
                                                                    <i class="far fa-eye"></i>
                                                                </a>
                                                            @endif
                                                            </h4>
                                                        </div>
                                                        <div class="d-block">
                                                            <p>
                                                                <b>
                                                                comentario:<br>
                                                                </b>
                                                                {{ $calificaciones->comentario }}
                                                            </p>
                                                            <small class="muted float-right">{{ date( 'd-m-Y G:i', strtotime($calificaciones->created_at)) }}</small>
                                                        </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            @else
                                                <h4>No lo han calificado todav&iacute;a</h4>
                                            @endif
                                    </div>
                                </div>
                            </div>
                            
                            <form action="{{route('reviews.store')}}" method="post">
                                @csrf   
                                
                            </form>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection


