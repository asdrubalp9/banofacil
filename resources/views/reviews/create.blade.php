@extends('layouts.app')

@section('content')
<div class="container py-4">
    <div class="row justify-content-center">
        <div class="col-md-11">
            <div class="card">
                <div class="card-header">
                    Califica al usuario
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-10 mx-auto ">
                            <div class="row">
                                <div class="col-md-2 col-sm-6">
                                    <img class="avatar img-thumbnail rounded-circle" src="{{ url('/storage/').'/'.$userInfo->url }}" style="max-width:130px; height:130px">    
                                </div>
                                <div class="col-md-8 ">
                                    <div class="card-body">
                                        <form action="{{route('reviews.store')}}" method="post">
                                            @csrf   
                                            <input type="hidden" name="id" value="{{$id}}">
                                            <input type="hidden" name="transaccion" value="{{$trans}}">
                                            <input type="hidden" name="tipo" value="{{$tipo}}">
                                            <div class="form-group">
                                                <label>
                                                    Que tal fue la experiencia con {{ $userInfo->name }}?
                                                </label>
                                                
                                                    <br>
                                                <label>
                                                    <input type="radio" name="rating" value="1" {{ ( old('rating' ) == 1 )?'checked':'' }}>
                                                    1/5<br>
                                                </label>
                                                <label>
                                                    <input type="radio" name="rating" value="2" {{ ( old('rating' ) == 2 )?'checked':'' }}>
                                                    2/5<br>
                                                </label>
                                                <label>
                                                    <input type="radio" name="rating" value="3" {{ ( old('rating' ) == 3 )?'checked':'' }}>
                                                    3/5<br>
                                                </label>
                                                <label>
                                                    <input type="radio" name="rating" value="4" {{ ( old('rating' ) == 4 )?'checked':'' }}>
                                                    4/5<br>
                                                </label>
                                                <label>
                                                    <input type="radio" name="rating" value="5" {{ ( old('rating' ) == 5 )?'checked':'' }}>
                                                    5/5<br>
                                                </label>
                                                @error('rating')
                                                    <span class="invalid-feedback" role="alert" >
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>    

                                            <div class="form-group">
                                                <label>Comentario adicional</label>
                                                <textarea name="comentario" type="text" class="form-control @error('comentario') is-invalid @enderror"  placeholder="Que tal fue la experiencia? el ba&ntilde;o estaba en buenas condiciones?" >{{ old('comentario') }}</textarea>
                                                @error('comentario')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>      
                                            <button class="btn btn-success mx-auto d-block">
                                                Calificar
                                            </button>
                                        </form>
                                    </div>
                                </div>  
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function () {
        $('.btn-success').on('click',function(e){
            e.preventDefault();
            Swal.fire({
                title: '¿Esta seguro desea realizar esta calificación?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si'
            }).then((result) => {
                if (result.value) {
                    $('form').submit();
                }
            })

        })
    });
</script>

@endsection