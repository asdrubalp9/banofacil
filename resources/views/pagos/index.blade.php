@extends('layouts.app')

@section('content')
<div class="container py-4">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">
                    Ingrese dinero a su cuenta
                    <a href="{{ route('transacciones') }}" class="btn btn-info float-right">
                        <i class="fas fa-wallet"></i>
                            {{ $saldoActual }} 
                    </a>
                    
                </div>
                <div class="card-body">
                    <form action="{{route('pagos.postAdd')}}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-md-6">
                                <h3>
                                    Monto a depositar
                                </h3>
                            <div class="form-group row">
                                <label for="monto" class="col-md-4 col-form-label text-md-right">
                                    Monto <small class="text-muted">| cantidad de dinero desea ingresar en la cuenta</small>
                                </label>

                                <div class="col-md-6">
                                    <div class="input-group mb-3">
                                        <input id="monto" type="number" min="0" class="form-control @error('monto') is-invalid @enderror" name="monto" value="{{ old('monto') }}" required placeholder="Monto" >
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="basic-addon2">CLP</span>
                                        </div>
                                    </div>

                                    @error('monto')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h3>
                                Confirmación  de transacción
                            </h3>
                            <div class="form-group row mb-0">
                                <div class="col-md-12 mx-auto">
                                <table>
                                    <tr>
                                        <td>
                                            <small class="text-small text-muted">
                                                Total a depositar:
                                            </small>
                                        </td>
                                        <td class="pl-3">
                                            <span id="preview">
                                                0
                                            </span>
                                             CLP
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <small class="text-small text-muted">
                                                Gastos administrativos/tarifa<br> de procesamiento (2.89%)
                                            </small>
                                        </td>
                                        <td class="pl-3">
                                            <span id="tax">
                                            0
                                            </span> CLP
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <small class="text-small text-danger">
                                                Total en tu cuenta
                                            </small>
                                        </td>
                                        <td class="pl-3 text-danger">
                                            <span id="total">
                                            0
                                            </span>
                                            CLP
                                        </td>
                                    </tr>
                                </table>
                                </div>
                            </div>      
                        </div>
                    </div>
                        
                                  
                        <div class="form-group row mb-0">
                            <div class="col-md-6 mx-auto text-center">
                                <button type="submit" class="btn btn-primary mt-3">
                                    Aceptar
                                </button>
                            </div>
                        </div>
                    </form>                    
                </div>
                
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $('#monto').keyup(function() {
            let monto =  $(this).val()
            let tax = Math.floor((monto * ( 2.89/ 100 )) + (monto * ( 2.89/ 100 ))*(19/100));
            let total = Math.floor(monto - tax )
            $('#tax').html(tax)
            $('#preview').html(monto)
            
            $('#total').html(total)
            //$reserva->costo * ( $config->monto_profit / 100 );
        });
        
    })
</script>
@endsection
