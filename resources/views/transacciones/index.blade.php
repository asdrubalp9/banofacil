@extends('layouts.app')

@section('content')
<div class="container py-4">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    
                        Historial de transacciones
                    <a href="{{ route('transacciones') }}" class="btn btn-info float-right">
                        <i class="fas fa-wallet"></i>
                            {{ $saldoActual }} 
                    </a>
                </div>
                @if( session()->has('alert') )
                <p class="text-center m-2">
                    {!! session('msg') !!}
                </p>
                @endif
                @if( count($fondos) > 0)
                    <table class="table">
                        <thead>
                            <tr>
                            <th scope="col">#</th>
                            <th scope="col">fecha</th>
                            <th scope="col">Tipo transaccion</th>
                            <th scope="col">monto</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($fondos as $fondo)    
                            <tr>
                            <td>
                                {{ $fondo->id }} 
                            </td>
                            <td>
                                {{ $fondo->created_at->format('G:m d-m-Y') }}  
                            </td>
                            <td>
                                {{ $fondo->tipo_transaccion }} 
                            </td>
                            <td>
                                {{ number_format($fondo->montoTransferencia,0,'','.') }} CLP
                            </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                @else
                    <h3 class="text-center m-3">
                        No posees fondos ni algún tipo de movimiento, 
                        <a href="{{ route('pagos.add') }}">
                            por que no empiezas agregando dinero a la cuenta? 
                        </a>
                    </h3>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection