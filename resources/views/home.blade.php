@extends('layouts.app')

@section('content')
<div class="container py-4">
    <div class="row justify-content-center">
        <div class="col-md-11">
            <div class="card">
                <div class="card-header">
                    Panel de control
                    <a href="{{ route('banos.crear') }}" class="btn btn-success">
                        Registrar ba&ntilde;o
                    </a>
                    <a href="{{ route('transacciones') }}" class="btn btn-info float-right">
                        <i class="fas fa-wallet"></i>
                            {{ $saldoActual }} 
                    </a>
                </div>
                <div class="card-body">
                <div class="row">
                        <div class="col-md-12">
                            <h3>
                                Ba&ntilde;os pendientes por usar
                            </h3>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                            @foreach( $user->transaccionesPagadas->where('confirmacion_pagador','=',0) as $transaccion)
                                <form method="post" action="{{ route('banos.usar',['id'=> $transaccion->bano->id ] )}}" class="col-md-4 list-group my-2 ">
                                    @csrf
                                    @include('reservas.listaReservas')
                                    @php    
                                        /*
                                        <!-- <div class="row">
                                            <div class="col-md-6">
                                                <button name="opc" class="btn btn-block btn-success">
                                                    Ya lo us&eacute;
                                                </button>
                                            </div>
                                            <div class="col-md-6">
                                                <button name="opc" class="btn btn-block btn-danger">
                                                    No podr&eacute; usarlo
                                                </button>
                                            </div>
                                        </div> -->
                                        */
                                    @endphp
                                </form>
                            @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <h3>
                                Personas a las que le debes prestar el ba&ntilde;o
                            </h3>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                @foreach( $user->transaccionesRecibidas->where('confirmacion_pagador','=',0) as $transaccion)
                                <form method="post" action="{{ route('banos.prestar',['id'=> $transaccion->bano_id ] )}}" class="col-md-4 list-group my-2">
                                        @csrf
                                        <ul class="list-group col-md-12">
                                            <li class="list-group-item">
                                                <div class="row">
                                                    @php
                                                        $img = $transaccion->usuarioPagador->image()->where('image_type','=',3)->first();              
                                                            if($img){
                                                                echo '<div class="col-md-6">';
                                                    @endphp                                        
                                                                    <img class="avatar img-thumbnail rounded-circle" src="{{ url('/storage/').'/'.$img->url }}" style="max-width:130px; height:130px">
                                                    @php
                                                            echo '</div>';
                                                            }
                                                    @endphp
                                                    <div class="{{ ( $img )?'col-md-6':'col-md-12' }} pt-4">
                                                        Nombre: {{ $transaccion->usuarioPagador->name }}<br>
                                                        Genero: {{ $transaccion->usuarioPagador->genero }}<br>

                                                        Tel&eacute;fono: {{ $transaccion->usuarioPagador->telefono }}
                                                    </div>
                                                </div>
                                                
                                            </li>
                                            <li class="list-group-item">
                                                Fecha de la reserva: {{ date('d-m-Y G:i', strtotime($transaccion->fecha_reserva) ) }}
                                            </li>
                                            <li class="list-group-item">
                                                costo: {{ number_format($transaccion->costo,0,'','.') }} CLP
                                            </li>
                                            
                                            <li class="list-group-item">
                                                Bano a usar: {{ $transaccion->bano->direccion }}
                                            </li>
                                            
                                            @if($transaccion->confirmacion_pagador)
                                                <li class="list-group-item">
                                                    El usuario ya realizo su calificaci&oacute;n
                                                </li>
                                            @endif
                                            <li class="list-group-item">
                                                <div class="row pt-3">
                                                    <div class="col-md-6">
                                                    <button name="opc" class="btn btn-block btn-success" name="usado" value="{{ $transaccion->reserva_id }}">
                                                            Ya fu&eacute; usado
                                                        </button>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <button name="opc" class="btn btn-block btn-danger">
                                                            No lo prestar&eacute;
                                                        </button>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>          
                                    </form>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
