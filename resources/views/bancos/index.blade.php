@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    Cuenta bancaria
                </div>
                <div class="card-body">
                    <form action="{{route('bancos.store')}}" method="post"  autocomplete="off" enctype="multipart/form-data">
                        @csrf
                        @include('bancos.form')
                        <div class="row">
                            <div class="col-md-3 d-block mx-auto my-3">
                                <button class="btn btn-success btn-block" type="submit">
                                    Guardar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        /*
        !function(n,e,o){function s(n,e){return typeof n===e}function a(){var n,e,o,a,i,f,r;for(var c in l)if(l.hasOwnProperty(c)){if(n=[],e=l[c],e.name&&(n.push(e.name.toLowerCase()),e.options&&e.options.aliases&&e.options.aliases.length))for(o=0;o<e.options.aliases.length;o++)n.push(e.options.aliases[o].toLowerCase());for(a=s(e.fn,"function")?e.fn():e.fn,i=0;i<n.length;i++)f=n[i],r=f.split("."),1===r.length?Modernizr[r[0]]=a:(!Modernizr[r[0]]||Modernizr[r[0]]instanceof Boolean||(Modernizr[r[0]]=new Boolean(Modernizr[r[0]])),Modernizr[r[0]][r[1]]=a),t.push((a?"":"no-")+r.join("-"))}}function i(n){var e=r.className,o=Modernizr._config.classPrefix||"";if(c&&(e=e.baseVal),Modernizr._config.enableJSClass){var s=new RegExp("(^|\\s)"+o+"no-js(\\s|$)");e=e.replace(s,"$1"+o+"js$2")}Modernizr._config.enableClasses&&(e+=" "+o+n.join(" "+o),c?r.className.baseVal=e:r.className=e)}var t=[],l=[],f={_version:"3.5.0",_config:{classPrefix:"",enableClasses:!0,enableJSClass:!0,usePrefixes:!0},_q:[],on:function(n,e){var o=this;setTimeout(function(){e(o[n])},0)},addTest:function(n,e,o){l.push({name:n,fn:e,options:o})},addAsyncTest:function(n){l.push({name:null,fn:n})}},Modernizr=function(){};Modernizr.prototype=f,Modernizr=new Modernizr;var r=e.documentElement,c="svg"===r.nodeName.toLowerCase();Modernizr.addTest("geolocation","geolocation"in navigator),a(),i(t),delete f.addTest,delete f.addAsyncTest;for(var u=0;u<Modernizr._q.length;u++)Modernizr._q[u]();n.Modernizr=Modernizr}(window,document);
        function errorCallback(error){
        switch(error.code) {
            case error.PERMISSION_DENIED:
                 console.log("El Usuario ha negado el acceso a la geolocalizacion");
                //x.innerHTML = "El Usuario ha negado el acceso a la geolocalizacion."
                break;
            case error.POSITION_UNAVAILABLE:
                 console.log("La informacion de ubicacion no esta disponible");
                //x.innerHTML = "La informacion de ubicacion no esta disponible."
                break;
            case error.TIMEOUT:
                 console.log("La solicitud para obtener la ubicación del usuario ha caducado.");
                //x.innerHTML = "La solicitud para obtener la ubicación del usuario ha caducado."
                break;
            case error.UNKNOWN_ERROR:
                 console.log("Ha ocurrido un error desconocido");
                //x.innerHTML = "An unknown error occurred."
                break;
        }
    }

    function successCallback(position) {
        document.getElementById('lat').value  = position.coords.latitude;
        document.getElementById('long').value = position.coords.longitude;
        
    }

        initMap();


      function initMap() {
        getGeo();
          var lat = $('#lat').val();
          var long = $('#long').val();
          var zoomy = 17;
          
          if( lat == '' && long == '' ){
            lat = -33.452250;
            long = -70.638173;
            zoomy = 13;
          }
          var styles = {
                    hide: [
                        {
                            featureType: 'poi',
                            stylers: [{visibility: 'off'}]
                        },
                        {
                            featureType: 'transit',
                            elementType: 'labels.icon',
                            stylers: [{visibility: 'off'}]
                        }
                    ]
                };
                
        var map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: lat, lng: long},
          zoom: zoomy,
        });
        map.setOptions({styles: styles['hide']});
        var card = document.getElementById('pac-card');
        var input = document.getElementById('Direccion');
        
        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.bindTo('bounds', map);
        autocomplete.setFields(
            ['address_components', 'geometry', 'icon', 'name']);
        var marker = new google.maps.Marker({
          map: map,
          anchorPoint: new google.maps.Point(0, -29)
        });

        autocomplete.addListener('place_changed', function() {
          marker.setVisible(false);
          var place = autocomplete.getPlace();
          if (!place.geometry) {
            $("#Direccion").addClass("is-invalid");
            $("#opc").removeClass("d-none");
            return;
          }else{
            $("#Direccion").addClass("is-valid");
                if (place.geometry.viewport) {
                    map.fitBounds(place.geometry.viewport);
                } else {
                    map.setCenter(place.geometry.location);
                    map.setZoom(17);
                }
                marker.setPosition(place.geometry.location);
                marker.setVisible(true);
                var address = '';
            if (place.address_components) {
                address = [
                (place.address_components[0] && place.address_components[0].short_name || ''),
                (place.address_components[1] && place.address_components[1].short_name || ''),
                (place.address_components[2] && place.address_components[2].short_name || '')
                ].join(' ');
            }
                $('#lat').val(place.geometry.location.lat());
                $('#long').val(place.geometry.location.lng());
          }
          
        });
      }

      function getGeo(){
        navigator.geolocation.getCurrentPosition(successCallback, errorCallback);
        if (Modernizr.geolocation) {
            navigator.geolocation.getCurrentPosition(successCallback, errorCallback);
        } else {
            Swal.fire({
                    title: 'Le recomendamos habilitar la geolocalizaci&oacute;n para que la p&aacute;gina funcione mejor',
                    showCancelButton: false,
                    showConfirmButton:true,        
                });
            
        }
    }
        getGeo();
// */
    });
</script>
<script  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCCK_rzXLZJDWf2TKkYC7zBwzamZC3yRP0&libraries=places"  ></script>
@include('partials.imageScript')
@endsection