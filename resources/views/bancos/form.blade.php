<label>
    Nombre del titular
</label>
<input name="nombre_titular" type="text" required class="form-control @error('nombre_titular') is-invalid @enderror" placeholder="Nombre del titular" value="{{ old('nombre_titular',$banco->nombre_titular ) }}">
@error('nombre_titular')
    <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
    </span>
@enderror

<label>
    Rut 
    <small class="text-muted">
        Ingresa 7 u 8 caracteres y escribe el guión antes del código verificador.
    </small>
</label>
<input name="rut" type="text" required class="form-control @error('rut') is-invalid @enderror" placeholder="RUT" value="{{ old('rut',$banco->rut ) }}">
@error('rut')
    <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
    </span>
@enderror


<label>
    Banco
</label>
<select name="banco" required class="form-control @error('banco') is-invalid @enderror">
    @if(old('banco',$banco->banco))
        <option name="{{ old('banco',$banco->banco ) }}">{{ old('banco',$banco->banco ) }}</option>
    @endif
    <option name="">Elige el banco</option>
    <option name="Banco Bice">Banco Bice</option>
    <option name="Banco Condell">Banco Condell</option>
    <option name="Banco Consorcio">Banco Consorcio</option>
    <option name="Banco de Chile">Banco de Chile</option>
    <option name="Banco de Crédito e Inversiones (BCI)">Banco de Crédito e Inversiones (BCI)</option>
    <option name="Banco del Desarrollo">Banco del Desarrollo</option>
    <option name="Banco Edwards Citibank">Banco Edwards Citibank</option>
    <option name="Banco Falabella">Banco Falabella</option>
    <option name="Banco Internacional">Banco Internacional</option>
    <option name="Banco Itau">Banco Itau</option>
    <option name="Banco Penta">Banco Penta</option>
    <option name="Banco Ripley">Banco Ripley</option>
    <option name="Banco Santander Chile">Banco Santander Chile</option>
    <option name="Banco Security">Banco Security</option>
    <option name="BancoEstado">BancoEstado</option>
    <option name="BBVA">BBVA</option>
    <option name="Corpbanca">Corpbanca</option>
    <option name="Credichile">Credichile</option>
    <option name="HSBC">HSBC</option>
    <option name="Santander Banefe">Santander Banefe</option>
    <option name="Scotiabank Sud Americano">Scotiabank Sud Americano</option>
</select>
@error('banco')
    <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
    </span>
@enderror


<label>
    Numero de cuenta
</label>
<input name="numero_de_cuenta" type="text" required class="form-control @error('numero_de_cuenta') is-invalid @enderror" placeholder="Nombre del titular" value="{{ old('numero_de_cuenta',$banco->numero_de_cuenta ) }}">
@error('numero_de_cuenta')
    <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
    </span>
@enderror

<label>
    Tipo de cuenta
</label>
<select name="tipo_de_cuenta" required class="form-control @error('tipo_de_cuenta') is-invalid @enderror" > 
    @if(old('tipo_de_cuenta',$banco->tipo_de_cuenta) )
        <option name="{{ old('tipo_de_cuenta',$banco->tipo_de_cuenta) }}">{{ old('tipo_de_cuenta',$banco->tipo_de_cuenta) }}</option>    
    @endif
    <option value="Seleccione el tipo de cuenta">Seleccione el tipo de cuenta</option>
    <option value="Cuenta vista">Cuenta vista</option>
    <option value="Cuenta Corriente">Cuenta corriente</option>
    <option value="Cuenta de ahorros">Cuenta de ahorros</option>
    <option value="Cuenta rut">Cuenta rut</option>
</select>
@error('tipo_de_cuenta')
    <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
    </span>
@enderror
