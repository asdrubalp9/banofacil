@if( session()->has('alert') )

<div class="stickyAlert">
    <div class="col-md-12">
        <div class="alert alert-{{ session('alert') }}" role="alert">
            {!! session('msg') !!}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
</div>
@endif


@if( $errors->any() )

<div class="stickyAlert">
    <div class="col-md-12">
        <div class="alert alert-danger" role="alert">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
</div>
@endif
<script>
$(document).ready(function(){
    setTimeout( "$('.stickyAlert').fadeOut();", 4000);

})
</script>