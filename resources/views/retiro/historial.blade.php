@extends('layouts.app')

@section('content')
<link href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" rel="stylesheet">

<script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>

<div class="container-fluid py-4">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Retirar dinero de su cuenta                    
                </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table ">
                                    <thead>
                                        <tr>
                                            <td>Nombre Usuario</td>
                                            <td>Email</td>
                                            <td>banco</td>
                                            <td>num cuenta</td>
                                            <td>tipo cuenta</td>
                                            <td>nombre titular</td>
                                            <td>monto</td>
                                            <td>numero de ref.</td>
                                            <td>Fecha Transferencia</td>
                                            <td>Numero Referencia</td>
                                            <td>Banco Emisor</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($datas as $data)
                                            @if( count($data->Retiros) > 0)
                                                @foreach($data->Retiros as $retiro)
                                                    <form method="post" >
                                                        @csrf
                                                        <tr>
                                                            <td>
                                                            {{ $data->name }}
                                                            
                                                            </td>
                                                            <td>
                                                            {{ $data->email }}
                                                            
                                                            </td>
                                                            <td>
                                                            {{ $retiro->nombre_banco }}
                                                            </td>
                                                            <td>
                                                            {{ $retiro->numero_cuenta }}
                                                            </td>
                                                            <td>
                                                            {{ $retiro->numero_cuenta }}
                                                            </td>
                                                            <td>
                                                            {{ $retiro->tipo_cuenta }}
                                                            </td>
                                                            <td>
                                                            {{ $retiro->rut }}
                                                            </td>                         
                                                            <td>
                                                            {{ $retiro->monto }}
                                                            </td>
                                                            <td>
                                                            {{ $retiro->numero_referencia }}
                                                            
                                                            </td>
                                                            <td>
                                                            {{ $retiro->transferido }}
                                                            
                                                            </td>
                                                            <td>
                                                                {{ $retiro->banco_emisor }}
                                                            </td>
                                                            
                                                        </tr>
                                                    </form>
                                                @endforeach

                                            @endif
                                            
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){

        $('#monto').keyup(function() {
            let monto =  $(this).val()
            let tax = Math.floor((monto * ( 2.89/ 100 )) + (monto * ( 2.89/ 100 ))*(19/100));
            let total = Math.floor(monto - tax )
            $('#tax').html(tax)
            $('#preview').html(monto)
            
            $('#total').html(total)
            //$reserva->costo * ( $config->monto_profit / 100 );
        });

        $(".table").DataTable();
        
    })
</script>
@endsection
