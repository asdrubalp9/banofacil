@extends('layouts.app')

@section('content')
<link href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" rel="stylesheet">

<script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<div class="container-fluid py-4">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Retirar dinero de su cuenta
                </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table ">
                                    <thead>
                                        <tr>
                                            <td>Nombre Usuario</td>
                                            <td>banco</td>
                                            <td>num cuenta</td>
                                            <td>tipo cuenta</td>
                                            <td>nombre titular</td>
                                            <td>rut</td>
                                            <td>SaldoActual</td>
                                            <td>Monto Transferencia</td>
                                            <td>Fecha Transferencia</td>
                                            <td>Numero Referencia</td>
                                            <td>Banco Emisor</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($retiros as $retiro)
                                            @if(count($retiro->cuentas) && isset($retiro->misFondos->last()->saldoActual) && $retiro->misFondos->last()->saldoActual > 500)
                                            <form action="{{ route('retiros.add') }}" method="post" >
                                                @csrf
                                                <tr>
                                                    <td>{{ $retiro->name }}</td>
                                                    <td>{{ $retiro->cuentas[0]->banco }}</td>
                                                    <td>{{ $retiro->cuentas[0]->numero_de_cuenta }}</td>
                                                    <td>{{ $retiro->cuentas[0]->tipo_de_cuenta }}</td>
                                                    <td>{{ $retiro->cuentas[0]->nombre_titular }}</td>
                                                    <td>{{ $retiro->cuentas[0]->rut }}</td>                         
                                                    <td>{{ $retiro->misFondos->last()->saldoActual }}</td>
                                                    <td>
                                                        <input type="hidden" name="user_id" value="{{$retiro->id}}" >
                                                        <input type="number" min="0" class="form-control" name="monto" value="{{ $retiro->misFondos->last()->saldoActual }}">
                                                    </td>
                                                    <td>
                                                        <input type="date" class="form-control" name="fecha_transferencia" value="{{ date('Y-m-d')}}">
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control" name="numero_referencia" value="">
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control" name="banco_emisor" value="">
                                                    </td>
                                                    <td>
                                                        <button class="btn btn-xs btn-info">
                                                            Registrar
                                                        </button>
                                                    </td>
                                                </tr>
                                            </form>
                                            @endif
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){

        $('#monto').keyup(function() {
            let monto =  $(this).val()
            let tax = Math.floor((monto * ( 2.89/ 100 )) + (monto * ( 2.89/ 100 ))*(19/100));
            let total = Math.floor(monto - tax )
            $('#tax').html(tax)
            $('#preview').html(monto)
            
            $('#total').html(total)
            //$reserva->costo * ( $config->monto_profit / 100 );
        });
        $(".table").DataTable();
        
    })
</script>
@endsection
